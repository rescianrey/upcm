# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0022_auto_20141124_1337'),
    ]

    operations = [
        migrations.AlterField(
            model_name='education',
            name='survey_data',
            field=models.ForeignKey(related_name='education', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_volunteer',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
    ]
