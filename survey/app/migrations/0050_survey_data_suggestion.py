# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0049_auto_20150212_0324'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='suggestion',
            field=models.CharField(max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
    ]
