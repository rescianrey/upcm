# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0077_auto_20150310_0249'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='number_of_others',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
