# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0025_survey_data_specialty_at_time_of_entry_others'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='birth_order_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='guardian',
            name='relationship',
            field=models.CharField(max_length=50, choices=[(b'mother', b'Mother'), (b'father', b'Father'), (b'guardian', b'Guardian')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='guardian',
            name='survey_data',
            field=models.ForeignKey(related_name='guardians', to='app.Survey_Data'),
            preserve_default=True,
        ),
    ]
