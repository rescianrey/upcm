# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='relative_with_stocks',
            name='is_outside_PH',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='board_certified',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='did_rural_living_interest_change',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='did_rural_practice_interest_change',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='had_rural_training',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='had_training_outside_PH',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_health_profession_relatives',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_received_scholarship',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_relatives_in_politics',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_relatives_with_business',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_relatives_with_stocks_in_PH',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_relatives_with_stocks_outside_PH',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_role_model_mentor',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='have_second_degree_relatives_outside_PH',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_accepted_in_UPCM_program',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_accepted_in_indigenous_program',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_in_a_relationship',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_petitioned',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_primary_occupation_medical',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_volunteer',
            field=models.BooleanField(default=False),
        ),
    ]
