# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0070_auto_20150220_0458'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='rural_experience_description',
            field=models.TextField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
