# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20141031_0955'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='child_school',
            options={'verbose_name': 'Child School'},
        ),
        migrations.AlterModelOptions(
            name='education',
            options={'verbose_name': 'Education', 'verbose_name_plural': 'Education'},
        ),
        migrations.AlterModelOptions(
            name='relative_in_health_profession',
            options={'verbose_name': 'Relative in Health Profession', 'verbose_name_plural': 'Relatives in Health Profession'},
        ),
        migrations.AlterModelOptions(
            name='relative_in_politics',
            options={'verbose_name': 'Relative in Politics', 'verbose_name_plural': 'Relatives in Politics'},
        ),
        migrations.AlterModelOptions(
            name='relative_outside_ph',
            options={'verbose_name': 'Relative Outside PH', 'verbose_name_plural': 'Relatives Outside PH'},
        ),
        migrations.AlterModelOptions(
            name='relative_with_business',
            options={'verbose_name': 'Relative with Business', 'verbose_name_plural': 'Relatives with Business'},
        ),
        migrations.AlterModelOptions(
            name='relative_with_stocks',
            options={'verbose_name': 'Relative with Stocks', 'verbose_name_plural': 'Relatives with Stocks'},
        ),
        migrations.AlterModelOptions(
            name='survey_data',
            options={'verbose_name': 'Survey Data', 'verbose_name_plural': 'Survey Data'},
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='petition_description',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
