# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0048_auto_20150212_0314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guardian',
            name='work_location',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
