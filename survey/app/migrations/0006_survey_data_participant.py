# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20141101_1057'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='participant',
            field=models.ForeignKey(blank=True, to='app.Participant', null=True),
            preserve_default=True,
        ),
    ]
