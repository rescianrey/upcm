# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0074_auto_20150220_0512'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='number_of_house',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
