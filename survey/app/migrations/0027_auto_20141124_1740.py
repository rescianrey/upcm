# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0026_auto_20141124_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='source_of_income_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relative_in_health_profession',
            name='survey_data',
            field=models.ForeignKey(related_name='relatives_in_health_profession', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relative_in_politics',
            name='survey_data',
            field=models.ForeignKey(related_name='relatives_in_politics', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relative_outside_ph',
            name='survey_data',
            field=models.ForeignKey(related_name='relatives_outside_ph', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relative_with_business',
            name='survey_data',
            field=models.ForeignKey(related_name='relatives_with_business', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relative_with_stocks',
            name='survey_data',
            field=models.ForeignKey(related_name='relatives_with_stocks', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sibling',
            name='survey_data',
            field=models.ForeignKey(related_name='siblings', to='app.Survey_Data'),
            preserve_default=True,
        ),
    ]
