# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0059_survey_data_other_specialty'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Specialty',
        ),
    ]
