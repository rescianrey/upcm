# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0058_auto_20150217_2120'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='other_specialty',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
