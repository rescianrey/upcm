# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0068_survey_data_scholarship_classification'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='scholarship_classification_other',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
