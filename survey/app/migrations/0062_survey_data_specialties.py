# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0061_specialty'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='specialties',
            field=models.ManyToManyField(to='app.Specialty'),
            preserve_default=True,
        ),
    ]
