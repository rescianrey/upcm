# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_participant_mobile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='mobile',
            field=models.CharField(max_length=13, validators=[django.core.validators.RegexValidator(regex=b'^\\d{13}$', message=b'Mobile number must be numeric. No spaces.', code=b'invalid_mobile')]),
            preserve_default=True,
        ),
    ]
