# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0030_auto_20141125_0606'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='number_of_second_degree_relatives_outside_PH',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='years_lived_in_region',
            field=models.CharField(blank=True, max_length=5, null=True, choices=[(b'0', b'0'), (b'1-3', b'1-3'), (b'4-6', b'4-6'), (b'7-10', b'7-10'), (b'>10', b'>10')]),
            preserve_default=True,
        ),
    ]
