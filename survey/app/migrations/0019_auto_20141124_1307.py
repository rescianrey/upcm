# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_auto_20141124_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='is_petitioned',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_primary_occupation_medical',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
    ]
