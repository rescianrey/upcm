# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0028_survey_data_internship_type_other'),
    ]

    operations = [
        migrations.AddField(
            model_name='child_school',
            name='school_work_location',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
    ]
