# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0076_auto_20150310_0236'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='number_of_car',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='number_of_motorcycle',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='number_of_van',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
