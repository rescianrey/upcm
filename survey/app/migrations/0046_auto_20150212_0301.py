# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0045_education_level'),
    ]

    operations = [
        migrations.AlterField(
            model_name='education',
            name='scholarhip_type',
            field=models.CharField(blank=True, max_length=50, null=True, choices=[(b'Academic-based', b'Academic-based'), (b'Non-academic (athletic)', b'Non-academic (athletic)'), (b'Non-academic (cultural)', b'Non-academic (cultural)'), (b'Non-academic (other)', b'Non-academic (other)')]),
            preserve_default=True,
        ),
    ]
