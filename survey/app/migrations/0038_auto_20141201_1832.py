# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0037_auto_20141201_0945'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='sex',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, max_length=3, null=True, choices=[(b'M', b'Male'), (b'F', b'Female')]),
            preserve_default=True,
        ),
    ]
