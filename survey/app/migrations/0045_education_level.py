# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0044_auto_20150212_0241'),
    ]

    operations = [
        migrations.AddField(
            model_name='education',
            name='level',
            field=models.CharField(default='', max_length=200, choices=[(b'Elementary', b'Elementary'), (b'High School', b'High School'), (b'College', b'College'), (b'Post Graduate', b'Post Graduate')]),
            preserve_default=False,
        ),
    ]
