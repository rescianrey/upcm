# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0041_auto_20150210_0401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='year_graduated',
            field=models.CharField(max_length=4, choices=[(b'1990', b'1990'), (b'1991', b'1991'), (b'1992', b'1992'), (b'1993', b'1993'), (b'1994', b'1994'), (b'1995', b'1995'), (b'1996', b'1996'), (b'1997', b'1997'), (b'1998', b'1998'), (b'1999', b'1999'), (b'2000', b'2000'), (b'2001', b'2001'), (b'2002', b'2002'), (b'2003', b'2003'), (b'2004', b'2004'), (b'2005', b'2005'), (b'2006', b'2006'), (b'2007', b'2007'), (b'2008', b'2008'), (b'2009', b'2009'), (b'2010', b'2010')], null=True, blank=True, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')]),
            preserve_default=True,
        ),
    ]
