# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20141124_1307'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='primary_occupation_nature_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
