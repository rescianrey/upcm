# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0035_auto_20141125_0843'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='appropriate_specialty_for_rural_medical_practice_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='interest_in_rural_life',
            field=models.IntegerField(blank=True, null=True, choices=[(1, b'Very Weak'), (2, b'Weak'), (3, b'Moderate'), (4, b'Strong'), (5, b'Very Strong')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='interest_in_rural_medical_practice',
            field=models.IntegerField(blank=True, null=True, choices=[(1, b'Very Weak'), (2, b'Weak'), (3, b'Moderate'), (4, b'Strong'), (5, b'Very Strong')]),
            preserve_default=True,
        ),
    ]
