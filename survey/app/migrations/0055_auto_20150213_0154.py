# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0054_auto_20150213_0148'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='survey_data',
            name='specialty',
        ),
        migrations.RemoveField(
            model_name='survey_data',
            name='specialty_others',
        ),
        migrations.AddField(
            model_name='survey_data',
            name='specialty_at_time_of_entry',
            field=models.CharField(max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='specialty_at_time_of_entry_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
