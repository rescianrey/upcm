# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_survey_data_participant'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='citizenship_desc',
            field=models.CharField(max_length=50, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='citizenship',
            field=models.CharField(max_length=30, choices=[(b'Filipino', b'Filipino'), (b'Other', b'Other'), (b'Dual', b'Dual')]),
            preserve_default=True,
        ),
    ]
