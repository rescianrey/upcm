# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0011_auto_20141117_1026'),
    ]

    operations = [
        migrations.AddField(
            model_name='participant',
            name='mobile',
            field=models.CharField(default=None, max_length=12, validators=[django.core.validators.RegexValidator(regex=b'^\\d{12}$', message=b'Mobile number must be numeric. No spaces.', code=b'invalid_mobile')]),
            preserve_default=False,
        ),
    ]
