# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0047_survey_data_applied_for_in_indigenous_program'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='applied_for_in_indigenous_program',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
    ]
