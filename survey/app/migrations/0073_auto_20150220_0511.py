# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0072_auto_20150220_0504'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='internship_type',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'straight', b'Straight Internship'), (b'rotating', b'Rotating Internship'), (b'other', b'Other')]),
            preserve_default=True,
        ),
    ]
