# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0034_survey_data_leisure_activities_others'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='reasons_for_taking_medicine_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='frequency_of_travel',
            field=models.CharField(blank=True, max_length=5, null=True, choices=[(b'0', b'0'), (b'1-3', b'1-3'), (b'4-6', b'4-6'), (b'7-10', b'7-10'), (b'>10', b'>10')]),
            preserve_default=True,
        ),
    ]
