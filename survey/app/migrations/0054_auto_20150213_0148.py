# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0053_auto_20150213_0141'),
    ]

    operations = [
        migrations.RenameField(
            model_name='survey_data',
            old_name='specialty_at_time_of_entry_others',
            new_name='specialty_others',
        ),
        migrations.RemoveField(
            model_name='survey_data',
            name='specialty_at_time_of_entry',
        ),
        migrations.AddField(
            model_name='survey_data',
            name='specialty',
            field=models.ManyToManyField(to='app.Specialty', null=True, blank=True),
            preserve_default=True,
        ),
    ]
