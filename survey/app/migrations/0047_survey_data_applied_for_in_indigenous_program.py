# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0046_auto_20150212_0301'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='applied_for_in_indigenous_program',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
