# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0052_specialty'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='specialty_at_time_of_entry',
            field=models.ManyToManyField(to='app.Specialty', null=True, blank=True),
            preserve_default=True,
        ),
    ]
