# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20141102_1700'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='appropriate_specialty_for_rural_medical_practice',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='birth_date',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='birth_order',
            field=models.CharField(max_length=50, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='birth_place',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='citizenship',
            field=models.CharField(blank=True, max_length=8, null=True, choices=[(b'Filipino', b'Filipino'), (b'Other', b'Other'), (b'Dual', b'Dual')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='civil_status',
            field=models.CharField(blank=True, max_length=2, null=True, choices=[(b'S', b'Single'), (b'M', b'Married'), (b'SP', b'Separated'), (b'W', b'Widow/er')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='civil_status_at_entry',
            field=models.CharField(blank=True, max_length=2, null=True, choices=[(b'S', b'Single'), (b'M', b'Married'), (b'SP', b'Separated'), (b'W', b'Widow/er')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='compassion_rating',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='current_address',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='family_annual_income',
            field=models.CharField(blank=True, max_length=30, null=True, choices=[(b'>3,000,000', b'>3,000,000'), (b'1,000,001-3,000,000', b'1,000,001-3,000,000'), (b'500,001-1,000,000', b'500,001-1,000,000'), (b'250,001-500,000', b'250,001-500,000'), (b'135,001-250,000', b'135,001-250,000'), (b'>Poverty threshold-135,000', b'>Poverty threshold-135,000'), (b'0-Poverty threshold', b'0-Poverty threshold')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='frequency_of_travel',
            field=models.CharField(blank=True, max_length=5, null=True, choices=[(b'>10', b'>10'), (b'7-10', b'7-10'), (b'4-6', b'4-6'), (b'1-3', b'1-3'), (b'0', b'0')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='interest_in_rural_life',
            field=models.IntegerField(blank=True, null=True, choices=[(5, b'Very Strong'), (4, b'Strong'), (3, b'Moderate'), (2, b'Weak'), (1, b'Very Weak')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='interest_in_rural_medical_practice',
            field=models.IntegerField(blank=True, null=True, choices=[(5, b'Very Strong'), (4, b'Strong'), (3, b'Moderate'), (2, b'Weak'), (1, b'Very Weak')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='internship_type',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_inclined_to_medical_practice_reason',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='leisure_activities',
            field=models.CharField(max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='native_language_proficiency',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='participant_practice_location_influence',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='permanent_address',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='physician_practice_location_influence',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='planned_practice_location',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'PH urban', b'Philippines (urban)'), (b'PH rural', b'Philippines (rural'), (b'outside PH', b'Outside the Philippines')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='properties_owned',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='reasons_for_taking_medicine',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='relatives_residence_address',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='sex',
            field=models.CharField(blank=True, max_length=1, null=True, choices=[(b'M', b'Male'), (b'F', b'Female')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='socio_economic_level',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[(b'Low', b'Low'), (b'Low Middle', b'Low Middle'), (b'Middle', b'Middle'), (b'Upper Middle', b'Upper Middle'), (b'High', b'High')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='source_of_income',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='specialty_at_time_of_entry',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='stfap_bracket',
            field=models.CharField(max_length=2, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='vehicles_owned',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='year_graduated',
            field=models.CharField(blank=True, max_length=4, null=True, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='years_lived_in_region',
            field=models.CharField(blank=True, max_length=5, null=True, choices=[(b'>10', b'>10'), (b'7-10', b'7-10'), (b'4-6', b'4-6'), (b'1-3', b'1-3'), (b'0', b'0')]),
            preserve_default=True,
        ),
    ]
