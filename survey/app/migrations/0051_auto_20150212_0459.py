# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0050_survey_data_suggestion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='suggestion',
            field=models.TextField(max_length=500, null=True, blank=True),
            preserve_default=True,
        ),
    ]
