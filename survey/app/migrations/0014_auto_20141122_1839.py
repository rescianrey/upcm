# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20141122_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='mobile',
            field=models.CharField(max_length=13, validators=[django.core.validators.RegexValidator(regex=b'^\\+\\d{12}$', message=b'Mobile number must be numeric. No spaces.', code=b'invalid_mobile')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='participant',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
