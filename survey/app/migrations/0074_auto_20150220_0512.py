# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0073_auto_20150220_0511'),
    ]

    operations = [
        migrations.RenameField(
            model_name='survey_data',
            old_name='internship_type_description',
            new_name='internship_department',
        ),
    ]
