# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0064_auto_20150218_0135'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='survey_data',
            name='leisure_activities',
        ),
        migrations.RemoveField(
            model_name='survey_data',
            name='leisure_activities_others',
        ),
    ]
