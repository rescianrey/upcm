# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0032_auto_20141125_0726'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='vehicles_owned_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='guardian',
            name='relationship',
            field=models.CharField(max_length=50, choices=[(None, b''), (b'mother', b'Mother'), (b'father', b'Father'), (b'guardian', b'Guardian')]),
            preserve_default=True,
        ),
    ]
