# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0017_auto_20141122_2302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='child_school',
            name='survey_data',
            field=models.ForeignKey(related_name='children', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='occupation',
            name='survey_data',
            field=models.ForeignKey(related_name='occupations', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='survey_data',
            name='is_primary_occupation_medical',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(b'yes', b'Yes'), (b'no', b'No'), (None, b'-')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='training',
            name='survey_data',
            field=models.ForeignKey(related_name='trainings', to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='training',
            name='training_type',
            field=models.CharField(max_length=50, choices=[(b'residency', b'Residency'), (b'fellowship', b'Fellowship')]),
            preserve_default=True,
        ),
    ]
