# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Child_School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('child_birth_order', models.IntegerField()),
                ('age', models.IntegerField()),
                ('level', models.CharField(max_length=50)),
                ('school_type', models.CharField(max_length=7, choices=[(b'public', b'Public'), (b'private', b'Private')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Education',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('school_name', models.CharField(max_length=50)),
                ('location', models.CharField(max_length=200)),
                ('school_type', models.CharField(max_length=7, choices=[(b'public', b'Public'), (b'private', b'Private')])),
                ('scholarhip_type', models.CharField(max_length=50, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Guardian',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(max_length=50)),
                ('occupation', models.CharField(max_length=50)),
                ('work_location', models.CharField(max_length=200, null=True, blank=True)),
                ('residence_address', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Occupation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=100)),
                ('inclusive_years_from', models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')])),
                ('inclusive_years_to', models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')])),
                ('location', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relative_In_Health_Profession',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(max_length=50)),
                ('occupation', models.CharField(max_length=50)),
                ('location', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relative_In_Politics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(max_length=50)),
                ('position_held', models.CharField(max_length=50)),
                ('location', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relative_Outside_PH',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(max_length=50)),
                ('location', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relative_With_Business',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(max_length=50)),
                ('nature_of_business', models.CharField(max_length=100)),
                ('business_size', models.CharField(max_length=50)),
                ('location', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Relative_With_Stocks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relationship', models.CharField(max_length=50)),
                ('hospital_location', models.CharField(max_length=200)),
                ('is_outside_PH', models.BooleanField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sibling',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('birth_order', models.IntegerField()),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('age', models.IntegerField()),
                ('school_or_occupation', models.CharField(max_length=50)),
                ('school_or_occupation_location', models.CharField(max_length=200)),
                ('school_type', models.CharField(max_length=8, choices=[(b'public', b'Public'), (b'private', b'Private')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Spouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('years_married', models.IntegerField()),
                ('occupation', models.CharField(max_length=50)),
                ('occupation_location', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Survey_Data',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('sex', models.CharField(max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female')])),
                ('birth_date', models.DateField()),
                ('birth_place', models.CharField(max_length=200)),
                ('citizenship', models.CharField(max_length=30)),
                ('current_address', models.CharField(max_length=200)),
                ('permanent_address', models.CharField(max_length=200)),
                ('civil_status', models.CharField(max_length=2, choices=[(b'S', b'Single'), (b'M', b'Married'), (b'SP', b'Separated'), (b'W', b'Widow/er')])),
                ('number_of_children', models.IntegerField(null=True, blank=True)),
                ('year_graduated', models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')])),
                ('board_certified', models.BooleanField()),
                ('is_primary_occupation_medical', models.BooleanField()),
                ('primary_occupation_specialization', models.CharField(max_length=100, null=True, blank=True)),
                ('primary_occupation_nature', models.CharField(max_length=500, null=True, blank=True)),
                ('non_medical_occupation_nature', models.CharField(max_length=200, null=True, blank=True)),
                ('non_medical_occupation_employer', models.CharField(max_length=100, null=True, blank=True)),
                ('non_medical_occupation_location', models.CharField(max_length=200, null=True, blank=True)),
                ('physician_practice_location_influence', models.CharField(max_length=200)),
                ('participant_practice_location_influence', models.CharField(max_length=200)),
                ('civil_status_at_entry', models.CharField(max_length=2, choices=[(b'S', b'Single'), (b'M', b'Married'), (b'SP', b'Separated'), (b'W', b'Widow/er')])),
                ('is_in_a_relationship', models.BooleanField()),
                ('person_in_relationship_with_address', models.CharField(max_length=200, null=True, blank=True)),
                ('spouse_origin_address', models.CharField(max_length=200, null=True, blank=True)),
                ('is_petitioned', models.BooleanField()),
                ('petition_description', models.CharField(max_length=50)),
                ('is_volunteer', models.BooleanField()),
                ('volunteer_work_nature', models.CharField(max_length=200, null=True, blank=True)),
                ('volunteer_work_roles', models.CharField(max_length=200, null=True, blank=True)),
                ('volunteer_work_location', models.CharField(max_length=200, null=True, blank=True)),
                ('planned_practice_location', models.CharField(max_length=10, choices=[(b'PH urban', b'Philippines (urban)'), (b'PH rural', b'Philippines (rural'), (b'outside PH', b'Outside the Philippines')])),
                ('specialty_at_time_of_entry', models.CharField(max_length=200)),
                ('is_accepted_in_UPCM_program', models.BooleanField()),
                ('is_accepted_in_indigenous_program', models.BooleanField()),
                ('years_lived_in_region', models.CharField(max_length=5, choices=[(b'>10', b'>10'), (b'7-10', b'7-10'), (b'4-6', b'4-6'), (b'1-3', b'1-3'), (b'0', b'0')])),
                ('native_language_proficiency', models.CharField(max_length=200)),
                ('number_of_brothers', models.IntegerField(null=True, blank=True)),
                ('number_of_sisters', models.IntegerField(null=True, blank=True)),
                ('birth_order', models.CharField(max_length=50)),
                ('have_second_degree_relatives_outside_PH', models.BooleanField()),
                ('number_of_second_degree_relatives_outside_PH', models.IntegerField(default=0)),
                ('relatives_residence_address', models.CharField(max_length=200)),
                ('have_health_profession_relatives', models.BooleanField()),
                ('have_relatives_with_stocks_in_PH', models.BooleanField()),
                ('have_relatives_with_stocks_outside_PH', models.BooleanField()),
                ('have_relatives_with_business', models.BooleanField()),
                ('have_relatives_in_politics', models.BooleanField()),
                ('source_of_income', models.CharField(max_length=100)),
                ('family_annual_income', models.CharField(max_length=30, choices=[(b'>3,000,000', b'>3,000,000'), (b'1,000,001-3,000,000', b'1,000,001-3,000,000'), (b'500,001-1,000,000', b'500,001-1,000,000'), (b'250,001-500,000', b'250,001-500,000'), (b'135,001-250,000', b'135,001-250,000'), (b'>Poverty threshold-135,000', b'>Poverty threshold-135,000'), (b'0-Poverty threshold', b'0-Poverty threshold')])),
                ('socio_economic_level', models.CharField(max_length=20, choices=[(b'Low', b'Low'), (b'Low Middle', b'Low Middle'), (b'Middle', b'Middle'), (b'Upper Middle', b'Upper Middle'), (b'High', b'High')])),
                ('stfap_bracket', models.CharField(max_length=2)),
                ('properties_owned', models.CharField(max_length=100)),
                ('vehicles_owned', models.CharField(max_length=100)),
                ('frequency_of_travel', models.CharField(max_length=5, choices=[(b'>10', b'>10'), (b'7-10', b'7-10'), (b'4-6', b'4-6'), (b'1-3', b'1-3'), (b'0', b'0')])),
                ('leisure_activities', models.CharField(max_length=500)),
                ('reasons_for_taking_medicine', models.CharField(max_length=200)),
                ('compassion_rating', models.IntegerField()),
                ('interest_in_rural_life', models.IntegerField(choices=[(5, b'Very Strong'), (4, b'Strong'), (3, b'Moderate'), (2, b'Weak'), (1, b'Very Weak')])),
                ('interest_in_rural_medical_practice', models.IntegerField(choices=[(5, b'Very Strong'), (4, b'Strong'), (3, b'Moderate'), (2, b'Weak'), (1, b'Very Weak')])),
                ('appropriate_specialty_for_rural_medical_practice', models.CharField(max_length=100)),
                ('have_received_scholarship', models.BooleanField()),
                ('had_rural_training', models.BooleanField()),
                ('rural_traning_type', models.CharField(blank=True, max_length=15, null=True, choices=[(b'Required', b'Required course'), (b'Elective', b'Elective course'), (b'Extracurricular', b'Extracurricular')])),
                ('rural_experience_description', models.CharField(max_length=200, null=True, blank=True)),
                ('have_role_model_mentor', models.BooleanField()),
                ('role_model_mentor_position', models.CharField(max_length=100, null=True, blank=True)),
                ('role_model_mentor_location', models.CharField(max_length=200, null=True, blank=True)),
                ('internship_type', models.CharField(max_length=100)),
                ('internship_type_description', models.CharField(max_length=100, null=True, blank=True)),
                ('had_training_outside_PH', models.BooleanField()),
                ('training_outside_PH_location', models.CharField(max_length=200, null=True, blank=True)),
                ('did_rural_living_interest_change', models.BooleanField()),
                ('rural_living_interest_change_reason', models.CharField(max_length=100, null=True, blank=True)),
                ('rural_living_interest_change_when', models.CharField(blank=True, max_length=15, null=True, choices=[(b'during training', b'During training'), (b'after training', b'After training')])),
                ('did_rural_practice_interest_change', models.BooleanField()),
                ('rural_practice_interest_change_reason', models.CharField(max_length=100, null=True, blank=True)),
                ('rural_practice_interest_change_when', models.CharField(blank=True, max_length=15, null=True, choices=[(b'during training', b'During training'), (b'after training', b'After training')])),
                ('is_inclined_to_medical_practice', models.BooleanField(default=True)),
                ('is_inclined_to_medical_practice_reason', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Training',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', models.CharField(max_length=200)),
                ('inclusive_years_from', models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')])),
                ('inclusive_years_to', models.CharField(max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')])),
                ('training_type', models.CharField(max_length=50)),
                ('survey_data', models.ForeignKey(to='app.Survey_Data')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='spouse',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sibling',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relative_with_stocks',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relative_with_business',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relative_outside_ph',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relative_in_politics',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relative_in_health_profession',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='occupation',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='guardian',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='education',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='child_school',
            name='survey_data',
            field=models.ForeignKey(to='app.Survey_Data'),
            preserve_default=True,
        ),
    ]
