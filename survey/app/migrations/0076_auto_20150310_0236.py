# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0075_survey_data_number_of_house'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='number_of_condo',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='number_of_residential_land',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
