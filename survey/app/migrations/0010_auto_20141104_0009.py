# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_auto_20141103_2242'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='spouse',
            name='survey_data',
        ),
        migrations.DeleteModel(
            name='Spouse',
        ),
        migrations.AddField(
            model_name='survey_data',
            name='spouse_occupation',
            field=models.CharField(max_length=50, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='spouse_occupation_location',
            field=models.CharField(max_length=200, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='year_of_marriage',
            field=models.CharField(blank=True, max_length=4, null=True, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')]),
            preserve_default=True,
        ),
    ]
