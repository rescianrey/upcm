# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0071_auto_20150220_0503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='have_role_model_mentor',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
    ]
