# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0069_survey_data_scholarship_classification_other'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='had_rural_training',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
    ]
