# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0040_auto_20150105_1215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='socio_economic_level',
            field=models.CharField(blank=True, max_length=20, null=True, choices=[(b'Low', b'Low (PHP62,000)'), (b'Low Middle', b'Low Middle (PHP191,000)'), (b'Middle', b'Middle (Middle PHP603,000)'), (b'Upper Middle/High', b'Upper Middle/High (PHP 1,857,000)')]),
            preserve_default=True,
        ),
    ]
