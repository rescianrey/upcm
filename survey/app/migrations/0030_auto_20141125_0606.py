# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0029_child_school_school_work_location'),
    ]

    operations = [
        migrations.RenameField(
            model_name='survey_data',
            old_name='volunteer_work_location_pthers',
            new_name='volunteer_work_location_others',
        ),
        migrations.AlterField(
            model_name='child_school',
            name='school_type',
            field=models.CharField(max_length=7, choices=[(None, b''), (b'public', b'Public'), (b'private', b'Private')]),
            preserve_default=True,
        ),
    ]
