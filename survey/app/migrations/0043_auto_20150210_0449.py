# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0042_auto_20150210_0420'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='primary_occupation_specialization',
            field=models.CharField(blank=True, max_length=100, null=True, choices=[(b'Administration', b'Administration'), (b'Anesthesiology', b'Anesthesiology'), (b'Community Medicine', b'Community Medicine'), (b'Dermatology', b'Dermatology'), (b'Emergency Medicine', b'Emergency Medicine'), (b'Epidemiology', b'Epidemiology'), (b'Family Medicine', b'Family Medicine'), (b'Internal Med', b'Internal Med'), (b'Neurology', b'Neurology'), (b'Nuclear Med', b'Nuclear Med'), (b'OB/Gyne', b'OB/Gyne'), (b'Ophthalmology', b'Ophthalmology'), (b'Orthopedics', b'Orthopedics'), (b'Otorhinolaryngology', b'Otorhinolaryngology'), (b'Pathology', b'Pathology'), (b'Pediatrics', b'Pediatrics'), (b'Psychiatry', b'Psychiatry'), (b'Public Health', b'Public Health'), (b'Radiology', b'Radiology'), (b'Rehabilitation', b'Rehabilitation'), (b'Surgery', b'Surgery'), (b'Academe/Teaching', b'Academe/Teaching'), (b'Research', b'Research')]),
            preserve_default=True,
        ),
    ]
