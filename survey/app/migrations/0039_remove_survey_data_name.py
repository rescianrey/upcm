# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0038_auto_20141201_1832'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='survey_data',
            name='name',
        ),
    ]
