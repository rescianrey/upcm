# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0066_auto_20150218_0136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey_data',
            name='have_received_scholarship',
            field=models.CharField(blank=True, max_length=3, null=True, choices=[(None, b'-'), (b'yes', b'Yes'), (b'no', b'No')]),
            preserve_default=True,
        ),
    ]
