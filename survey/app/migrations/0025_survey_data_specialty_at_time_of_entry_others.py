# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0024_auto_20141124_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='specialty_at_time_of_entry_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
