# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_participant'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='participant',
            name='user',
        ),
        migrations.AddField(
            model_name='participant',
            name='email',
            field=models.EmailField(default=None, max_length=75),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='token',
            field=models.CharField(default=None, max_length=10),
            preserve_default=False,
        ),
    ]
