# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0055_auto_20150213_0154'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='survey_data',
            name='specialty_at_time_of_entry',
        ),
        migrations.RemoveField(
            model_name='survey_data',
            name='specialty_at_time_of_entry_others',
        ),
    ]
