# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0062_survey_data_specialties'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='specialty',
            options={'verbose_name_plural': 'Specialties'},
        ),
        migrations.RemoveField(
            model_name='survey_data',
            name='leisure_activities',
        ),
        migrations.RemoveField(
            model_name='survey_data',
            name='leisure_activities_others',
        ),
    ]
