# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0067_auto_20150220_0444'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='scholarship_classification',
            field=models.CharField(blank=True, max_length=10, null=True, choices=[(b'academic', b'Academic'), (b'athletic', b'Non-Academic, Athletic'), (b'cultural', b'Non-Academic, Cultural'), (b'other', b'Non-Academic, Other')]),
            preserve_default=True,
        ),
    ]
