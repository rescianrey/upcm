# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0065_auto_20150218_0135'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='leisure_activities',
            field=models.ManyToManyField(to='app.Leisure'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='leisure_activities_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
