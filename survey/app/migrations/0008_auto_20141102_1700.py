# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20141102_1611'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='spouse',
            name='years_married',
        ),
        migrations.AddField(
            model_name='spouse',
            name='year_of_marriage',
            field=models.CharField(default=None, max_length=4, validators=[django.core.validators.RegexValidator(regex=b'^\\d{4}$', message=b'Year must be numeric.', code=b'invalid_year')]),
            preserve_default=False,
        ),
    ]
