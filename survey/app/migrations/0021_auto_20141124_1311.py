# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0020_survey_data_primary_occupation_nature_others'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey_data',
            name='participant_practice_location_influence_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='survey_data',
            name='physician_practice_location_influence_others',
            field=models.CharField(max_length=100, null=True, blank=True),
            preserve_default=True,
        ),
    ]
