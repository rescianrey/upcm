function addressInterface(fieldID){
    var DELIMITER = ", ";
    jQuery("#"+fieldID).focus(function(){
        jQuery("div.addr-fieldset-cont").remove();
        var fid = fieldID;
        var fieldValues = jQuery("#"+ fid).val().split(DELIMITER);

        fieldValues[0] = (fieldValues[0] == null?"":fieldValues[0]);
        fieldValues[1] = (fieldValues[1] == null?"":fieldValues[1]);
        fieldValues[2] = (fieldValues[2] == null?"":fieldValues[2]);
        fieldValues[3] = (fieldValues[3] == null?"":fieldValues[3]);

        var fieldsetContainer = jQuery("<div style='position: relative; display:inline-block; z-index:1000; width: 0; height: 0' class='addr-fieldset-cont'>");

        var fieldset = jQuery("<fieldset id='"+ fid +"-addr-fieldset'>");
        fieldset.addClass("module aligned address");

        // Street
        var streetRow = jQuery("<div>");
        streetRow.addClass("control-group form-row");

        var streetRowInnerDiv = jQuery("<div>");
        var streetRowLabel = jQuery("<div>");
        streetRowLabel.addClass("control-label");
        streetRowLabel.append("<label for='"+ fid +"-id_place_street'>Street</label>");
        streetRowInnerDiv.append(streetRowLabel);

        var streetRowControl = jQuery("<div>");
        streetRowControl.addClass("controls");
        streetRowControl.append("<input id='"+ fid +"-id_place_street' maxlength='200' name='street' type='text' value='"+ fieldValues[0] +"'>");
        streetRowInnerDiv.append(streetRowControl);
        streetRow.append(streetRowInnerDiv);
        fieldset.append(streetRow);

        // City
        var cityRow = jQuery("<div>");
        cityRow.addClass("control-group form-row");

        var cityRowInnerDiv = jQuery("<div>");
        var cityRowLabel = jQuery("<div>");
        cityRowLabel.addClass("control-label");
        cityRowLabel.append("<label for='"+ fid +"-id_place_city'>City</label>");
        cityRowInnerDiv.append(cityRowLabel);

        var cityRowControl = jQuery("<div>");
        cityRowControl.addClass("controls");
        cityRowControl.append("<input id='"+ fid +"-id_place_city' maxlength='200' name='city' type='text' value='"+ fieldValues[1] +"'>");
        cityRowInnerDiv.append(cityRowControl);
        cityRow.append(cityRowInnerDiv);
        fieldset.append(cityRow);

        // Province
        var provinceRow = jQuery("<div>");
        provinceRow.addClass("control-group form-row");

        var provinceRowInnerDiv = jQuery("<div>");
        var provinceRowLabel = jQuery("<div>");
        provinceRowLabel.addClass("control-label");
        provinceRowLabel.append("<label for='"+ fid +"-id_place_province'>Province</label>");
        provinceRowInnerDiv.append(provinceRowLabel);

        var provinceRowControl = jQuery("<div>");
        provinceRowControl.addClass("controls");
        provinceRowControl.append("<input id='"+ fid +"-id_place_province' maxlength='200' name='province' type='text' value='"+ fieldValues[2] +"'>");
        provinceRowInnerDiv.append(provinceRowControl);
        provinceRow.append(provinceRowInnerDiv);
        fieldset.append(provinceRow);

        // Country
        var countryRow = jQuery("<div>");
        countryRow.addClass("control-group form-row");

        var countryRowInnerDiv = jQuery("<div>");
        var countryRowLabel = jQuery("<div>");
        countryRowLabel.addClass("control-label");
        countryRowLabel.append("<label for='"+ fid +"-id_place_country'>Country</label>");
        countryRowInnerDiv.append(countryRowLabel);

        var countryRowControl = jQuery("<div>");
        countryRowControl.addClass("controls");

        var selectBox = jQuery("<select id='"+ fid +"-id_place_country' name='country' value='"+ fieldValues[3] +"'>");

        for(var i in COUNTRIES){
            selectBox.append("<option "+ (COUNTRIES[i]=="Philippines"?"selected":"") +" >" + COUNTRIES[i] + "</option>");
        }

        countryRowControl.append(selectBox);
        countryRowControl.append("<div id='"+ fid +"-addr-done-btn' style='position: relative; width: 0; height: 0'><button type='button' class='btn btn-high btn-info addr-done-btn'>Done</button></div>");
        countryRowInnerDiv.append(countryRowControl);
        countryRow.append(countryRowInnerDiv);
        fieldset.append(countryRow);

        fieldsetContainer.append(fieldset);
        jQuery("#" + fid).after(fieldsetContainer);
        jQuery("#"+ fid +"-addr-done-btn").click(function(){
            var fValues = [
                jQuery("#" + fid +"-id_place_street").val(),
                jQuery("#" + fid +"-id_place_city").val(),
                jQuery("#" + fid +"-id_place_province").val(),
                jQuery("#" + fid +"-id_place_country").val()
            ];
            jQuery("#" + fid).val(fValues.join(DELIMITER));
            jQuery("#" + fid +"-addr-fieldset").remove();
            return false;
        });
        jQuery("#" + fid +"-id_place_street").focus();
    });
}

var COUNTRIES = [
    'Afghanistan',
    'Aland Islands (Finland)',
    'Albania',
    'Algeria',
    'American Samoa (USA)',
    'Andorra',
    'Angola',
    'Anguilla (UK)',
    'Antigua and Barbuda',
    'Argentina',
    'Armenia',
    'Aruba (Netherlands)',
    'Australia',
    'Austria',
    'Azerbaijan',
    'Bahamas',
    'Bahrain',
    'Bangladesh',
    'Barbados',
    'Belarus',
    'Belgium',
    'Belize',
    'Benin',
    'Bermuda (UK)',
    'Bhutan',
    'Bolivia',
    'Bosnia and Herzegovina',
    'Botswana',
    'Brazil',
    'British Virgin Islands (UK)',
    'Brunei',
    'Bulgaria',
    'Burkina Faso',
    'Burma',
    'Burundi',
    'Cambodia',
    'Cameroon',
    'Canada',
    'Cape Verde',
    'Caribbean Netherlands (Netherlands)',
    'Cayman Islands (UK)',
    'Central African Republic',
    'Chad',
    'Chile',
    'China',
    'Christmas Island (Australia)',
    'Cocos (Keeling) Islands (Australia)',
    'Colombia',
    'Comoros',
    'Cook Islands (NZ)',
    'Costa Rica',
    'Croatia',
    'Cuba',
    'Curacao (Netherlands)',
    'Cyprus',
    'Czech Republic',
    'Democratic Republic of the Congo',
    'Denmark',
    'Djibouti',
    'Dominica',
    'Dominican Republic',
    'Ecuador',
    'Egypt',
    'El Salvador',
    'Equatorial Guinea',
    'Eritrea',
    'Estonia',
    'Ethiopia',
    'Falkland Islands (UK)',
    'Faroe Islands (Denmark)',
    'Federated States of Micronesia',
    'Fiji',
    'Finland',
    'France',
    'French Guiana (France)',
    'French Polynesia (France)',
    'Gabon',
    'Gambia',
    'Georgia',
    'Germany',
    'Ghana',
    'Gibraltar (UK)',
    'Greece',
    'Greenland (Denmark)',
    'Grenada',
    'Guadeloupe (France)',
    'Guam (USA)',
    'Guatemala',
    'Guernsey (UK)',
    'Guinea',
    'Guinea-Bissau',
    'Guyana',
    'Haiti',
    'Honduras',
    'Hong Kong (China)',
    'Hungary',
    'Iceland',
    'India',
    'Indonesia',
    'Iran',
    'Iraq',
    'Ireland',
    'Isle of Man (UK)',
    'Israel',
    'Italy',
    'Ivory Coast',
    'Jamaica',
    'Japan',
    'Jersey (UK)',
    'Jordan',
    'Kazakhstan',
    'Kenya',
    'Kiribati',
    'Kosovo',
    'Kuwait',
    'Kyrgyzstan',
    'Laos',
    'Latvia',
    'Lebanon',
    'Lesotho',
    'Liberia',
    'Libya',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macau (China)',
    'Macedonia',
    'Madagascar',
    'Malawi',
    'Malaysia',
    'Maldives',
    'Mali',
    'Malta',
    'Marshall Islands',
    'Martinique (France)',
    'Mauritania',
    'Mauritius',
    'Mayotte (France)',
    'Mexico',
    'Moldov',
    'Monaco',
    'Mongolia',
    'Montenegro',
    'Montserrat (UK)',
    'Morocco',
    'Mozambique',
    'Namibia',
    'Nauru',
    'Nepal',
    'Netherlands',
    'New Caledonia (France)',
    'New Zealand',
    'Nicaragua',
    'Niger',
    'Nigeria',
    'Niue (NZ)',
    'Norfolk Island (Australia)',
    'North Korea',
    'Northern Mariana Islands (USA)',
    'Norway',
    'Oman',
    'Pakistan',
    'Palau',
    'Palestine',
    'Panama',
    'Papua New Guinea',
    'Paraguay',
    'Peru',
    'Philippines',
    'Pitcairn Islands (UK)',
    'Poland',
    'Portugal',
    'Puerto Rico',
    'Qatar',
    'Republic of the Congo',
    'Reunion (France)',
    'Romania',
    'Russia',
    'Rwanda',
    'Saint Barthelemy (France)',
    'Saint Helena, Ascension and Tristan da Cunha (UK)',
    'Saint Kitts and Nevis',
    'Saint Lucia',
    'Saint Martin (France)',
    'Saint Pierre and Miquelon (France)',
    'Saint Vincent and the Grenadines',
    'Samoa',
    'San Marino',
    'Sao Tom and Principe',
    'Saudi Arabia',
    'Senegal',
    'Serbia',
    'Seychelles',
    'Sierra Leone',
    'Singapore',
    'Sint Maarten (Netherlands)',
    'Slovakia',
    'Slovenia',
    'Solomon Islands',
    'Somalia',
    'South Africa',
    'South Korea',
    'South Sudan',
    'Spain',
    'Sri Lanka',
    'Sudan',
    'Suriname',
    'Svalbard and Jan Mayen (Norway)',
    'Swaziland',
    'Sweden',
    'Switzerland',
    'Syria',
    'Taiwan',
    'Tajikistan',
    'Tanzania',
    'Thailand',
    'Timor-Leste',
    'Togo',
    'Tokelau (NZ)',
    'Tonga',
    'Trinidad and Tobago',
    'Tunisia',
    'Turkey',
    'Turkmenistan',
    'Turks and Caicos Islands (UK)',
    'Tuvalu',
    'Uganda',
    'Ukraine',
    'United Arab Emirates',
    'United Kingdom',
    'United States',
    'United States Virgin Islands (USA)',
    'Uruguay',
    'Uzbekistan',
    'Vanuatu',
    'Vatican City',
    'Venezuela',
    'Vietnam',
    'Wallis and Futuna (France)',
    'Western Sahara',
    'Yemen',
    'Zambia',
    'Zimbabwe'
]