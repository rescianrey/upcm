if(jQuery('#id_citizenship_2').is(':checked') || jQuery('#id_citizenship_1').is(':checked')){
    jQuery('#div-id_citizenship_desc').show();
}else{
    jQuery('#div-id_citizenship_desc').hide();
}

jQuery('input[name="citizenship"]:radio').change(function(){
    if(jQuery('#id_citizenship_2').is(':checked') || jQuery('#id_citizenship_1').is(':checked')){
        jQuery('#div-id_citizenship_desc').show();
    }else{
        jQuery('#div-id_citizenship_desc').hide();
    }
})

if(jQuery('#id_civil_status').val() == 'M'){
    jQuery('#div-id_year_of_marriage').show();
    jQuery('#div-id_spouse_occupation').show();
    jQuery('#div-id_spouse_occupation_location').show();
}else{
    jQuery('#div-id_year_of_marriage').hide();
    jQuery('#div-id_spouse_occupation').hide();
    jQuery('#div-id_spouse_occupation_location').hide();
}

jQuery('#id_civil_status').change(function(){
    if(jQuery('#id_civil_status').val() == 'M'){
        jQuery('#div-id_year_of_marriage').show();
        jQuery('#div-id_spouse_occupation').show();
        jQuery('#div-id_spouse_occupation_location').show();
    }else{
        jQuery('#div-id_year_of_marriage').hide();
        jQuery('#div-id_spouse_occupation').hide();
        jQuery('#div-id_spouse_occupation_location').hide();
    }
})

if(jQuery('#id_civil_status').val() == 'S'){
    jQuery('#div-id_number_of_children').hide();
    jQuery('#module-children').hide();
}else{
    jQuery('#div-id_number_of_children').show();
    jQuery('#module-children').show();
}

jQuery('#id_civil_status').change(function(){
    if(jQuery('#id_civil_status').val() == 'S'){
        jQuery('#div-id_number_of_children').hide();
        jQuery('#module-children').hide();
    }else{
        jQuery('#div-id_number_of_children').show();
        jQuery('#module-children').show();
    }
})

addressInterface("id_permanent_address");
addressInterface("id_current_address");

jQuery("input[id*=school_work_location]").each(function(){
    addressInterface(jQuery(this).attr("id"));
});

jQuery("a[id*='add-another']").click(function(){
    jQuery("input[id*=school_work_location]").each(function(){
        addressInterface(jQuery(this).attr("id"));
    });
});