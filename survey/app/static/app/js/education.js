if(jQuery('#id_civil_status_at_entry').val() == 'S'){
    jQuery('#div-id_is_in_a_relationship').show();
}else{
    jQuery('#div-id_is_in_a_relationship').hide();
}

if(jQuery('#id_is_petitioned').val() == 'yes'){
    jQuery('#div-id_immigrant_status').show();
    jQuery('#div-id_immigrant_country').show();
}else{
    jQuery('#div-id_immigrant_status').hide();
    jQuery('#div-id_immigrant_country').hide();
}

jQuery('#id_civil_status_at_entry').change(function(){
    if(jQuery('#id_civil_status_at_entry').val() == 'S'){
        jQuery('#div-id_is_in_a_relationship').show();
    }else{
        jQuery('#div-id_is_in_a_relationship').hide();
    }
})

if(jQuery('#id_is_in_a_relationship').val() == 'yes'){
    jQuery('#div-id_person_in_relationship_with_address').show();
}else{
    jQuery('#div-id_person_in_relationship_with_address').hide();
}

jQuery('#id_is_in_a_relationship').change(function(){
    if(jQuery('#id_is_in_a_relationship').val() == 'yes'){
        jQuery('#div-id_person_in_relationship_with_address').show();
    }else{
        jQuery('#div-id_person_in_relationship_with_address').hide();
    }
})

if(jQuery('#id_civil_status_at_entry').val() == 'M'){
    jQuery('#div-id_spouse_origin_address').show();
}else{
    jQuery('#div-id_spouse_origin_address').hide();
}

jQuery('#id_civil_status_at_entry').change(function(){
    if(jQuery('#id_civil_status_at_entry').val() == 'M'){
        jQuery('#div-id_spouse_origin_address').show();
    }else{
        jQuery('#div-id_spouse_origin_address').hide();
    }
})

if(jQuery('#id_is_petitioned').val() == 'yes'){
    jQuery('#div-id_petition_description').show();
}else{
    jQuery('#div-id_petition_description').hide();
}

jQuery('#id_is_petitioned').change(function(){
    if(jQuery('#id_is_petitioned').val() == 'yes'){
        jQuery('#div-id_petition_description').show();
    }else{
        jQuery('#div-id_petition_description').hide();
    }
})



if(jQuery('#id_is_volunteer').val() == 'yes'){
    jQuery('#div-id_volunteer_work_nature_0').show();
    jQuery('#div-id_volunteer_work_roles_0').show();
    jQuery('#div-id_volunteer_work_location_0').show();
}else{
    jQuery('#div-id_volunteer_work_nature_0').hide();
    jQuery('#div-id_volunteer_work_roles_0').hide();
    jQuery('#div-id_volunteer_work_location_0').hide();
}

jQuery('#id_is_volunteer').change(function(){
    if(jQuery('#id_is_volunteer').val() == 'yes'){
        jQuery('#div-id_volunteer_work_nature_0').show();
        jQuery('#div-id_volunteer_work_roles_0').show();
        jQuery('#div-id_volunteer_work_location_0').show();
    }else{
        jQuery('#div-id_volunteer_work_nature_0').hide();
        jQuery('#div-id_volunteer_work_roles_0').hide();
        jQuery('#div-id_volunteer_work_location_0').hide();
    }
})

if(jQuery('#id_volunteer_work_nature_6').is(':checked')){
    jQuery('#div-id_volunteer_work_nature_others').show()
}else{
    jQuery('#div-id_volunteer_work_nature_others').hide()
}

jQuery('input[name="volunteer_work_nature"]').change(function(){
    if(jQuery('#id_volunteer_work_nature_6').is(':checked')){
        jQuery('#div-id_volunteer_work_nature_others').show()
    }else{
        jQuery('#div-id_volunteer_work_nature_others').hide()
    }
})

if(jQuery('#id_volunteer_work_roles_2').is(':checked')){
    jQuery('#div-id_volunteer_work_roles_others').show()
}else{
    jQuery('#div-id_volunteer_work_roles_others').hide()
}

jQuery('input[name="volunteer_work_roles"]').change(function(){
    if(jQuery('#id_volunteer_work_roles_2').is(':checked')){
        jQuery('#div-id_volunteer_work_roles_others').show()
    }else{
        jQuery('#div-id_volunteer_work_roles_others').hide()
    }
})

if(jQuery('#id_volunteer_work_location_5').is(':checked')){
    jQuery('#div-id_volunteer_work_location_others').show()
}else{
    jQuery('#div-id_volunteer_work_location_others').hide()
}

jQuery('input[name="volunteer_work_location"]').change(function(){
    if(jQuery('#id_volunteer_work_location_5').is(':checked')){
        jQuery('#div-id_volunteer_work_location_others').show()
    }else{
        jQuery('#div-id_volunteer_work_location_others').hide()
    }
})
jQuery('#id_is_petitioned').change(function(){
    if(jQuery('#id_is_petitioned').val() == 'yes'){
        jQuery('#div-id_immigrant_status').show();
        jQuery('#div-id_immigrant_country').show();
    }else{
        jQuery('#div-id_immigrant_status').hide();
        jQuery('#div-id_immigrant_country').hide();
    }
});

jQuery("input[id*=-location]").each(function(){
    addressInterface(jQuery(this).attr("id"));
});