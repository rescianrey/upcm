if(jQuery('#id_applied_for_in_indigenous_program').val() == 'yes'){
    jQuery('#div-id_is_accepted_in_UPCM_program').show();
    jQuery('#div-id_is_accepted_in_indigenous_program').show();
    jQuery('#div-id_years_lived_in_region_0').show();
    jQuery('#div-id_native_language_proficiency_0').show();
}else{
    jQuery('#div-id_is_accepted_in_UPCM_program').hide();
    jQuery('#div-id_is_accepted_in_indigenous_program').hide();
    jQuery('#div-id_years_lived_in_region_0').hide();
    jQuery('#div-id_native_language_proficiency_0').hide();
}

jQuery('#id_applied_for_in_indigenous_program').change(function(){
    if(jQuery('#id_applied_for_in_indigenous_program').val() == 'yes'){
        jQuery('#div-id_is_accepted_in_UPCM_program').show();
        jQuery('#div-id_is_accepted_in_indigenous_program').show();
        jQuery('#div-id_years_lived_in_region_0').show();
        jQuery('#div-id_native_language_proficiency_0').show();
    }else{
        jQuery('#div-id_is_accepted_in_UPCM_program').hide();
        jQuery('#div-id_is_accepted_in_indigenous_program').hide();
        jQuery('#div-id_years_lived_in_region_0').hide();
        jQuery('#div-id_native_language_proficiency_0').hide();
    }
});