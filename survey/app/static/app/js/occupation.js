if(jQuery('#id_is_primary_occupation_medical').val() == 'yes'){
    jQuery('#div-id_primary_occupation_specialization').show();
    jQuery('#module-trainings').show();
    jQuery('#div-id_primary_occupation_nature_0').show();
    jQuery('#div-id_primary_occupation_nature_others').show();
    jQuery('#module-occupations').show()
}else{
    jQuery('#div-id_primary_occupation_specialization').hide();
    jQuery('#module-trainings').hide();
    jQuery('#div-id_primary_occupation_nature_0').hide();
    jQuery('#div-id_primary_occupation_nature_others').hide();
    jQuery('#module-occupations').hide()
}

jQuery('#id_is_primary_occupation_medical').change(function(){
    if(jQuery('#id_is_primary_occupation_medical').val() == 'yes'){
        jQuery('#div-id_primary_occupation_specialization').show();
        jQuery('#module-trainings').show();
        jQuery('#div-id_primary_occupation_nature_0').show();
        jQuery('#div-id_primary_occupation_nature_others').show();
        jQuery('#module-occupations').show()
    }else{
        jQuery('#div-id_primary_occupation_specialization').hide();
        jQuery('#module-trainings').hide();
        jQuery('#div-id_primary_occupation_nature_0').hide();
        jQuery('#div-id_primary_occupation_nature_others').hide();
        jQuery('#module-occupations').hide()
    }
})

if(jQuery('#id_primary_occupation_nature_4').is(':checked')){
    jQuery('#div-id_primary_occupation_nature_others').show()
}else{
    jQuery('#div-id_primary_occupation_nature_others').hide()
}

jQuery('input[name="primary_occupation_nature"]').change(function(){
    if(jQuery('#id_primary_occupation_nature_4').is(':checked')){
        jQuery('#div-id_primary_occupation_nature_others').show()
    }else{
        jQuery('#div-id_primary_occupation_nature_others').hide()
    }
})

if(jQuery('#id_is_primary_occupation_medical').val() == 'no'){
    jQuery('#div-id_non_medical_occupation_nature').show();
    jQuery('#div-id_non_medical_occupation_employer').show();
    jQuery('#div-id_non_medical_occupation_location').show();
}else{
    jQuery('#div-id_non_medical_occupation_nature').hide();
    jQuery('#div-id_non_medical_occupation_employer').hide();
    jQuery('#div-id_non_medical_occupation_location').hide();
}

jQuery('#id_is_primary_occupation_medical').change(function(){
    if(jQuery('#id_is_primary_occupation_medical').val() == 'no'){
        jQuery('#div-id_non_medical_occupation_nature').show();
        jQuery('#div-id_non_medical_occupation_employer').show();
        jQuery('#div-id_non_medical_occupation_location').show();
    }else{
        jQuery('#div-id_non_medical_occupation_nature').hide();
        jQuery('#div-id_non_medical_occupation_employer').hide();
        jQuery('#div-id_non_medical_occupation_location').hide();
    }
})

jQuery("input[id*=location]").each(function(){
    addressInterface(jQuery(this).attr("id"));
});

jQuery("a[id*='add-another']").click(function(){
    jQuery("input[id*=location]").each(function(){
        addressInterface(jQuery(this).attr("id"));
    });
});