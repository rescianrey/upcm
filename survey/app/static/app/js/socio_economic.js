function createDummyTextboxOnCheck(checkBoxID, fieldID){
    var dummy = jQuery('<input style="display:none; width: 30px; margin-left:10px;" type="number" />');
    jQuery('#'+checkBoxID).parent().append(dummy);

    jQuery('#'+checkBoxID).change(function(){
        if($(this).is(':checked')){
            dummy.show();
        }else{
            dummy.hide();
            dummy.val('');
            $('#'+fieldID).val('');
        }
    });

    if(jQuery('#'+checkBoxID).is(':checked')){
        dummy.show();
        dummy.val($('#'+fieldID).val());
    }else{
        dummy.hide();
        dummy.val('');
        $('#'+fieldID).val('');
    }

    dummy.change(function(){
        $('#'+fieldID).val(dummy.val());
    });
    $('#'+fieldID).parent().parent().hide();
}

createDummyTextboxOnCheck('id_properties_owned_0', 'id_number_of_house');
createDummyTextboxOnCheck('id_properties_owned_3', 'id_number_of_residential_land');
createDummyTextboxOnCheck('id_properties_owned_1', 'id_number_of_condo');

createDummyTextboxOnCheck('id_vehicles_owned_0', 'id_number_of_motorcycle');
createDummyTextboxOnCheck('id_vehicles_owned_1', 'id_number_of_car');
createDummyTextboxOnCheck('id_vehicles_owned_2', 'id_number_of_van');
createDummyTextboxOnCheck('id_vehicles_owned_4', 'id_number_of_others');


if(jQuery('#id_source_of_income_2').is(':checked')){
    jQuery('#div-id_source_of_income_others').show()
}else{
    jQuery('#div-id_source_of_income_others').hide()
}

jQuery('input[name="source_of_income"]').change(function(){
    if(jQuery('#id_source_of_income_2').is(':checked')){
        jQuery('#div-id_source_of_income_others').show()
    }else{
        jQuery('#div-id_source_of_income_others').hide()
    }
})

if(jQuery('#id_vehicles_owned_4').is(':checked')){
    jQuery('#div-id_vehicles_owned_others').show()
}else{
    jQuery('#div-id_vehicles_owned_others').hide()
}

jQuery('input[name="vehicles_owned"]').change(function(){
    if(jQuery('#id_vehicles_owned_4').is(':checked')){
        jQuery('#div-id_vehicles_owned_others').show()
    }else{
        jQuery('#div-id_vehicles_owned_others').hide()
    }
})

if(jQuery('#id_leisure_activities_8').is(':checked')){
    jQuery('#div-id_leisure_activities_others').show()
}else{
    jQuery('#div-id_leisure_activities_others').hide()
}

jQuery('input[name="leisure_activities"]').change(function(){
    if(jQuery('#id_leisure_activities_8').is(':checked')){
        jQuery('#div-id_leisure_activities_others').show()
    }else{
        jQuery('#div-id_leisure_activities_others').hide()
    }
})

jQuery('#id_stfap_bracket').after('&nbsp;&nbsp;[<a href="{{ STATIC_URL }}app/pdf/stfap_reference.pdf">See reference</a>]')
