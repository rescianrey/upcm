if(jQuery('#id_reasons_for_taking_medicine_7').is(':checked')){
    jQuery('#div-id_reasons_for_taking_medicine_others').show()
}else{
    jQuery('#div-id_reasons_for_taking_medicine_others').hide()
}

jQuery('input[name="reasons_for_taking_medicine"]').change(function(){
    if(jQuery('#id_reasons_for_taking_medicine_7').is(':checked')){
        jQuery('#div-id_reasons_for_taking_medicine_others').show()
    }else{
        jQuery('#div-id_reasons_for_taking_medicine_others').hide()
    }
})

if(jQuery('#id_appropriate_specialty_for_rural_medical_practice_4').is(':checked')){
    jQuery('#div-id_appropriate_specialty_for_rural_medical_practice_others').show()
}else{
    jQuery('#div-id_appropriate_specialty_for_rural_medical_practice_others').hide()
}

jQuery('input[name="appropriate_specialty_for_rural_medical_practice"]').change(function(){
    if(jQuery('#id_appropriate_specialty_for_rural_medical_practice_4').is(':checked')){
        jQuery('#div-id_appropriate_specialty_for_rural_medical_practice_others').show()
    }else{
        jQuery('#div-id_appropriate_specialty_for_rural_medical_practice_others').hide()
    }
})
limitSelection(3, "reasons_for_taking_medicine");