if(jQuery('#id_birth_order_3').is(':checked')){
    jQuery('#div-id_birth_order_others').show()
}else{
    jQuery('#div-id_birth_order_others').hide()
}

jQuery('input[name="birth_order"]').change(function(){
    if(jQuery('#id_birth_order_3').is(':checked')){
        jQuery('#div-id_birth_order_others').show()
    }else{
        jQuery('#div-id_birth_order_others').hide()
    }
})

if(jQuery('#id_have_second_degree_relatives_outside_PH').val() == 'yes'){
    jQuery('#div-id_number_of_second_degree_relatives_outside_PH').show();
    jQuery('#div-id_relatives_residence_address').show();
    jQuery('#module-relatives_outside_ph').show();
}else{
    jQuery('#div-id_number_of_second_degree_relatives_outside_PH').hide();
    jQuery('#div-id_relatives_residence_address').hide();
    jQuery('#module-relatives_outside_ph').hide();
}

jQuery('#id_have_second_degree_relatives_outside_PH').change(function(){
    if(jQuery('#id_have_second_degree_relatives_outside_PH').val() == 'yes'){
        jQuery('#div-id_number_of_second_degree_relatives_outside_PH').show();
        jQuery('#div-id_relatives_residence_address').show();
        jQuery('#module-relatives_outside_ph').show();
    }else{
        jQuery('#div-id_number_of_second_degree_relatives_outside_PH').hide();
        jQuery('#div-id_relatives_residence_address').hide();
        jQuery('#module-relatives_outside_ph').hide();
    }
})

if(jQuery('#id_have_health_profession_relatives').val() == 'yes'){
    jQuery('#module-relatives_in_health_profession').show();
}else{
    jQuery('#module-relatives_in_health_profession').hide();
}

jQuery('#id_have_health_profession_relatives').change(function(){
    if(jQuery('#id_have_health_profession_relatives').val() == 'yes'){
        jQuery('#module-relatives_in_health_profession').show();
    }else{
        jQuery('#module-relatives_in_health_profession').hide();
    }
})

if(jQuery('#id_have_relatives_with_stocks_in_PH').val() == 'yes' || jQuery('#id_have_relatives_with_stocks_outside_PH').val() == 'yes'){
    jQuery('#module-relatives_with_stocks').show();
}else{
    jQuery('#module-relatives_with_stocks').hide();
}

jQuery('#id_have_relatives_with_stocks_in_PH').change(function(){
        if(jQuery('#id_have_relatives_with_stocks_in_PH').val() == 'yes' || jQuery('#id_have_relatives_with_stocks_outside_PH').val() == 'yes'){
            jQuery('#module-relatives_with_stocks').show();
        }else{
            jQuery('#module-relatives_with_stocks').hide();
        }
})

jQuery('#id_have_relatives_with_stocks_outside_PH').change(function(){
        if(jQuery('#id_have_relatives_with_stocks_in_PH').val() == 'yes' || jQuery('#id_have_relatives_with_stocks_outside_PH').val() == 'yes'){
            jQuery('#module-relatives_with_stocks').show();
        }else{
            jQuery('#module-relatives_with_stocks').hide();
        }
})


if(jQuery('#id_have_relatives_with_business').val() == 'yes'){
    jQuery('#module-relatives_with_business').show();
}else{
    jQuery('#module-relatives_with_business').hide();
}

jQuery('#id_have_relatives_with_business').change(function(){
    if(jQuery('#id_have_relatives_with_business').val() == 'yes'){
        jQuery('#module-relatives_with_business').show();
    }else{
        jQuery('#module-relatives_with_business').hide();
    }
})

if(jQuery('#id_have_relatives_in_politics').val() == 'yes'){
    jQuery('#module-relatives_in_politics').show();
}else{
    jQuery('#module-relatives_in_politics').hide();
}

jQuery('#id_have_relatives_in_politics').change(function(){
    if(jQuery('#id_have_relatives_in_politics').val() == 'yes'){
        jQuery('#module-relatives_in_politics').show();
    }else{
        jQuery('#module-relatives_in_politics').hide();
    }
})

addressInterface("id_relatives_residence_address");

jQuery("input[id*=location]").each(function(){
    addressInterface(jQuery(this).attr("id"));
});