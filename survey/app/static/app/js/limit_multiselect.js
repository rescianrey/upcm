function limitCheck(field_id){
    var selectedCount_from = jQuery('option', jQuery('select#' + field_id + '_to')).size();
    var selectedCount_to = jQuery('select#' + field_id + '_from').val().length;
    var limit = parseInt(jQuery('select#' + field_id + '_from').attr('limit'));
    if(isNaN(limit) || selectedCount_from + selectedCount_to <= limit){
        SelectBox.move(field_id + '_from', field_id + '_to');
    }
}

