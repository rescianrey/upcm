limitSelection(3, 'physician_practice_location_influence');
limitSelection(3, 'participant_practice_location_influence');

if(jQuery('#id_physician_practice_location_influence_7').is(':checked')){
    jQuery('#div-id_physician_practice_location_influence_others').show()
}else{
    jQuery('#div-id_physician_practice_location_influence_others').hide()
}

jQuery('input[name="physician_practice_location_influence"]').change(function(){
    if(jQuery('#id_physician_practice_location_influence_7').is(':checked')){
        jQuery('#div-id_physician_practice_location_influence_others').show()
    }else{
        jQuery('#div-id_physician_practice_location_influence_others').hide()
    }
})

if(jQuery('#id_participant_practice_location_influence_7').is(':checked')){
    jQuery('#div-id_participant_practice_location_influence_others').show()
}else{
    jQuery('#div-id_participant_practice_location_influence_others').hide()
}

jQuery('input[name="participant_practice_location_influence"]').change(function(){
    if(jQuery('#id_participant_practice_location_influence_7').is(':checked')){
        jQuery('#div-id_participant_practice_location_influence_others').show()
    }else{
        jQuery('#div-id_participant_practice_location_influence_others').hide()
    }
})