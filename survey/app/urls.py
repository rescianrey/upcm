from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from app import views

urlpatterns = patterns('',
	url(r'^$', views.PersonalPage.as_view(), name='home'),
    url(r'^personal/$', views.PersonalPage.as_view(), name="personal"),
    url(r'^occupation/$', views.OccupationPage.as_view(), name="occupation"),
    url(r'^perceptions/$', views.PerceptionsPage.as_view(), name="perceptions"),
    url(r'^education/$', views.EducationPage.as_view(), name="education"),
    url(r'^plans/$', views.PlansPage.as_view(), name="plans"),
    url(r'^region-origin/$', views.Region_OriginPage.as_view(), name="region-origin"),
    url(r'^family/$', views.FamilyPage.as_view(), name="family"),
    url(r'^socio-economic/$', views.Socio_EconomicPage.as_view(), name="socio-economic"),
    url(r'^rural-life/$', views.Rural_LifePage.as_view(), name="rural-life"),
    url(r'^post-graduate/$', views.PostGraduateTrainingPage.as_view(), name="post-graduate"),
    url(r'^submission/$', views.SubmissionPage.as_view(), name="submission"),
    url(r'^completed/$', TemplateView.as_view(template_name='app/submission_successful.html'), name="completed"),

    # Registration
    url(r'^signup/$', views.SignUp.as_view(), name="signup"),

    # Login
    url(r'^login/$', views.Login.as_view(), name="login"),

    # Lost passcode
    url(r'^lost-passcode/$', views.LostPasscode.as_view(), name="lost-passcode"),

    # Logout
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="auth_logout"),
)