from import_export import resources
from import_export.admin import ExportMixin
from django.contrib import admin

from app.models import *

class ChildSchoolInline(admin.TabularInline):
    model = Child_School


class Survey_DataResource(resources.ModelResource):
    class Meta:
        model = Survey_Data

class Survey_DataAdmin(ExportMixin, admin.ModelAdmin):
    filter_horizontal = ('specialties',)
    resource_class = Survey_DataResource
	#inlines = [ChildSchoolInline, ]


class Child_SchoolResource(resources.ModelResource):
	class Meta:
		model = Child_School

class Child_SchoolAdmin(ExportMixin, admin.ModelAdmin):
	resource_class = Child_SchoolResource

class GuardianResource(resources.ModelResource):
	class Meta:
		model = Guardian

admin.site.register(Survey_Data, Survey_DataAdmin)
admin.site.register(Child_School, Child_SchoolAdmin)
admin.site.register(Guardian)
admin.site.register(Sibling)
admin.site.register(Training)
admin.site.register(Occupation)
admin.site.register(Education)
admin.site.register(Relative_With_Business)
admin.site.register(Relative_In_Health_Profession)
admin.site.register(Relative_In_Politics)
admin.site.register(Relative_With_Stocks)
admin.site.register(Relative_Outside_PH)
admin.site.register(Participant)
admin.site.register(Specialty)
admin.site.register(Leisure)