from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator

from app.models import *
from app.views_utils import activate_participant

# Author: Rescian Rey
# Description: Form for the Personal SurveyData
class Personal_SurveyDataForm(forms.ModelForm):
    required_fields = ['sex', 'birth_date', 'birth_place', 'citizenship', 'current_address', 
        'permanent_address', 'civil_status']

    def __init__(self, *args, **kwargs):
        super(Personal_SurveyDataForm, self).__init__(*args, **kwargs)
        # Setting required fields
        for f in self.required_fields:
            self.fields[f].required = True

        self.fields['sex'] = forms.ChoiceField(choices=Survey_Data.SEX_CHOICES, widget=forms.RadioSelect())
        self.fields['citizenship'] = forms.ChoiceField(choices=Survey_Data.CITIZENSHIP_CHOICES, widget=forms.RadioSelect())
        self.fields['current_address'].widget = forms.TextInput(attrs={'class': 'address-fields', 'placeholder' : 'City or Town/Province/Country'})
        self.fields['permanent_address'].widget = forms.TextInput(attrs={'class': 'address-fields', 'placeholder' : 'City or Town/Province/Country'})
        self.fields['spouse_occupation_location'].widget = forms.TextInput(attrs={'class': 'address-fields', 'placeholder' : 'City or Town/Province/Country'})
        self.fields['spouse_occupation_location'].label = 'Work location of your spouse'
        self.fields['spouse_occupation'].label = "Occupation of Spouse"
        self.fields['citizenship_desc'].label = 'Specify citizenhip'
        self.fields['citizenship_desc'].widget = forms.TextInput(attrs={'placeholder' : 'if Other, or Dual'})
        self.fields['birth_date'].widget = forms.TextInput(attrs={'type' : 'date'})
        self.fields['number_of_children'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
    class Meta:
        model = Survey_Data
        fields = ['sex', 'birth_date', 'birth_place', 'citizenship', 'citizenship_desc', 'current_address', 'permanent_address', 'civil_status', 
            'year_of_marriage', 'spouse_occupation', 'spouse_occupation_location', 'number_of_children']


# Author: Rescian Rey
# Description: Form for Child details
class Child_SchoolForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(Child_SchoolForm, self).__init__(*args, **kwargs)
        self.fields['child_birth_order'].widget = forms.NumberInput(attrs={'class': 'number-fields'})
        self.fields['child_birth_order'].label = 'Birth Order of Child'
        self.fields['level'].label = 'School Level/Occupation'
        self.fields['school_work_location'].label = 'School/Work Location'
        self.fields['age'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
        self.fields['school_type'].widget = forms.Select(attrs={'class' : 'short'}, choices=Child_School.SCHOOL_TYPE_CHOICES)

    class Meta:
        model = Child_School
        exclude = ('survey_data', )


# Author: Rescian Rey
# Description: Form for the Occupations SurveyData
class Occupation_SurveyDataForm(forms.ModelForm):
    PRIMARY_OCCUPATION_NATURE = (
        (1, 'Clinical Practice',),
        (2, 'Government Health Services'),
        (3, 'Medical Teaching'),
        (4, 'Health NGO Work'),
        (5, 'Others'),
    )

    def __init__(self, *args, **kwargs):
        super(Occupation_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['primary_occupation_nature'] = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=self.PRIMARY_OCCUPATION_NATURE)
        self.fields['year_graduated'].label = 'Year graduated from UPCM'
        self.fields['year_graduated'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YEAR_GRADUATED_CHOICES)
        self.fields['year_graduated'].required = True
        self.fields['is_primary_occupation_medical'].required = True
        self.fields['is_primary_occupation_medical'].label = 'Is primary occupation medical?'
        self.fields['is_primary_occupation_medical'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['primary_occupation_specialization'].label = 'Specialization or subspecialty'
        self.fields['primary_occupation_nature'].label = 'Nature of Medical Practice (Check all that applies)'
        self.fields['primary_occupation_nature_others'].label = 'If Others, specify'
        self.fields['non_medical_occupation_nature'].label = 'Nature of non-medical occupation'
        self.fields['non_medical_occupation_employer'].label = 'Present employer'
        self.fields['non_medical_occupation_location'].label = 'Location'
    class Meta:
        model = Survey_Data
        fields = ['year_graduated', 'board_certified', 'is_primary_occupation_medical', 'primary_occupation_specialization', 'primary_occupation_nature', 
        'primary_occupation_nature_others', 'non_medical_occupation_nature' , 'non_medical_occupation_employer',
        'non_medical_occupation_location', ]


class TrainingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TrainingForm, self).__init__(*args, **kwargs)
        self.fields['inclusive_years_from'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['inclusive_years_to'].widget = forms.TextInput(attrs={'class': 'short'})

    class Meta:
        model = Training
        fields = ['training_type', 'location', 'inclusive_years_from', 'inclusive_years_to']


class OccupationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(OccupationForm, self).__init__(*args, **kwargs)
        self.fields['inclusive_years_from'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['inclusive_years_to'].widget = forms.TextInput(attrs={'class': 'short'})

    class Meta:
        model = Occupation
        fields = ['description', 'location', 'inclusive_years_from', 'inclusive_years_to']


class Perceptions_SurveyDataForm(forms.ModelForm):
    INFLUENCE = (
        (1, 'Family'),
        (2, 'Lifestyle'),
        (3, 'Professional Advancement'),
        (4, 'Needs of the Community'),
        (5, 'Financial Security'),
        (6, 'Service'),
        (7, 'Safety'),
        (8, 'Others'),
    )

    def __init__(self, *args, **kwargs):
        super(Perceptions_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['physician_practice_location_influence'] = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=self.INFLUENCE, label="Which three factors do you think are the most influential to a physician's choice of practice location?")
        self.fields['physician_practice_location_influence_others'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['physician_practice_location_influence_others'].label = 'If Others, specify'

        self.fields['participant_practice_location_influence_others'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['participant_practice_location_influence_others'].label = 'If Others, specify'
        self.fields['participant_practice_location_influence'] = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=self.INFLUENCE, label="Which three factors most influenced you to practice in your current location?")

    class Meta:
        model = Survey_Data
        fields = ['physician_practice_location_influence', 'physician_practice_location_influence_others',
        'participant_practice_location_influence', 'participant_practice_location_influence_others']


class Education_SurveyDataForm(forms.ModelForm):
    VOLUNTEER_WORK_NATURE = (
            (1, 'Medical mission'),
            (2, 'Relief services'),
            (3, 'NGO activities (e.g. rallies/demos)'),
            (4, 'Community development'),
            (5, 'Community education'),
            (6, 'Community research'),
            (7, 'Others')
        )

    VOLUNTEER_WORK_ROLES = (
            (1, 'Organizer/leader'),
            (2, 'Participant / Member'),
            (3, 'Others'),
        )

    VOLUNTEER_WORK_LOCATION = (
            (1, 'Urban poor'),
            (2, 'Urban center'),
            (3, 'Rural'),
            (4, 'Indigenous tribes'),
            (5, 'Farmers/Fisherfolks'),
            (6, 'Others')
        )

    def __init__(self, *args, **kwargs):
        super(Education_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['volunteer_work_nature'] = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=self.VOLUNTEER_WORK_NATURE)
        self.fields['volunteer_work_roles'] = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=self.VOLUNTEER_WORK_ROLES)
        self.fields['volunteer_work_location'] = forms.MultipleChoiceField(required=False,
        widget=forms.CheckboxSelectMultiple, choices=self.VOLUNTEER_WORK_LOCATION)
        self.fields['civil_status_at_entry'].label = 'At the time of your entry to UPCM, what was your civil status?'
        self.fields['is_in_a_relationship'].label = 'Were you in a relationship?'
        self.fields['is_in_a_relationship'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['person_in_relationship_with_address'].label = 'Where did the person you had a relationship with reside at the time of your entry to UPCM?'
        self.fields['person_in_relationship_with_address'].widget = forms.TextInput(attrs={'class': 'address-fields', 'placeholder' : 'City or Town/Province/Country'})

        self.fields['spouse_origin_address'].label = 'Where is the location of origin of your spouse? '
        self.fields['spouse_origin_address'].widget = forms.TextInput(attrs={'class': 'address-fields', 'placeholder' : 'City or Town/Province/Country'})
        
        self.fields['is_petitioned'].label = 'At the time of entry to UPCM, were you petitioned or have you acquired an immigrant status in another country?'
        self.fields['is_petitioned'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)

        self.fields['is_volunteer'].label = 'During your pre-med years, were you involved in community, advocacy or volunteer work/service?'  
        self.fields['is_volunteer'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['volunteer_work_nature'].label = 'Nature of work/service'
        self.fields['volunteer_work_nature_others'].label = 'If Others, specify'
        self.fields['volunteer_work_roles'].label = 'Roles'
        self.fields['volunteer_work_roles_others'].label = 'If Others, specify'
        self.fields['volunteer_work_location'].label = 'Location'
        self.fields['volunteer_work_location_others'].label = 'If Others, specify'

    class Meta:
        model = Survey_Data
        fields = ['civil_status_at_entry', 'is_in_a_relationship', 'person_in_relationship_with_address', 'spouse_origin_address',
        'is_petitioned', 'immigrant_status', 'immigrant_country', 'is_volunteer', 'volunteer_work_nature', 'volunteer_work_nature_others',
        'volunteer_work_roles', 'volunteer_work_roles_others', 'volunteer_work_location', 'volunteer_work_location_others']


class EducationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EducationForm, self).__init__(*args, **kwargs)
        self.fields['school_type'].widget = forms.Select(attrs={'class' : 'short'}, choices=Child_School.SCHOOL_TYPE_CHOICES)
        self.fields['level'].widget = forms.Select(attrs={'class' : 'short'}, choices=Education.SCHOOL_LEVEL)
        self.fields['scholarhip_type'].widget = forms.Select(attrs={'class' : 'short'}, choices=Education.SCHOLARCHIP_TYPE)
    
    class Meta:
        model = Education
        exclude = ['survey_data',]


class Plans_SurveyDataForm(forms.ModelForm):

    SPECIALTY = (
            (1, 'Administration'),
            (2, 'Internal Medicine'),
            (3, 'Pediatrics'),
            (4, 'Academe/Teaching'),
            (5, 'Neurology'),
            (6, 'Public Health'),
            (7, 'Anesthesiology'),
            (8, 'Nuclear Medicine'),
            (9, 'Psychiatry'),
            (10, 'Community Medicine'),
            (11, 'Obstetrics and Gynecology'),
            (12, 'Radiology'),
            (13, 'Dermatology'),
            (14, 'Ophthalmology'),
            (15, 'Rehabilitation'),
            (16, 'Emergency Medicine'),
            (17, 'Orthopedics'),
            (18, 'Research'),
            (19, 'Epidemiology'),
            (20, 'Otorhinolaryngology'),
            (21, 'Surgery'),
            (22, 'Family Medicine'),
            (23, 'Pathology'),
            (24, 'Urology'),
            (25, 'Others'),
        )


    def __init__(self, *args, **kwargs):
        super(Plans_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['specialties'] = forms.ModelMultipleChoiceField(queryset=Specialty.objects.all(),
                                          label='At the time of your entry to UPCM, what was your specialty/ies of inclination? Please rank your top three ',
                                          required=False,
                                          widget=FilteredSelectMultiple(
                                                    'specialties',
                                                    is_stacked=False,
                                                    attrs={'limit' : '3', 'size':'10'}
                                                 ))
        self.fields['planned_practice_location'].label = 'At the time of your entry to UPCM, where were you planning to practice?'
        self.fields['other_specialty'].label = 'If Others, specify'

    class Meta:
        model = Survey_Data
        fields = ['planned_practice_location', 'specialties', 'other_specialty',]


class Region_Origin_SurveyDataForm(forms.ModelForm):

    PROFICIENCY = (
            ('speak-write', 'Could speak and write'),
            ('understand', 'Could not speak but could understand'),
            ('speak', 'Could speak but not write'),
            ('cannot understand', 'Could not understand')
        )

    def __init__(self, *args, **kwargs):
        super(Region_Origin_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['years_lived_in_region'] = forms.ChoiceField(choices=Survey_Data.YEARS_LIVED_IN_REGION_RANGE, widget=forms.RadioSelect())
        self.fields['native_language_proficiency'] = forms.ChoiceField(choices=self.PROFICIENCY, widget=forms.RadioSelect())
        self.fields['is_accepted_in_UPCM_program'].label = 'Were you accepted in the UPCM-Regionalization Program?'
        self.fields['is_accepted_in_indigenous_program'].label = 'Were you accepted in the Indigenous People Program?'
        self.fields['applied_for_in_indigenous_program'].label = 'Did you apply for the UPCM-Regionalization Program or Indigenous People Program?'
        self.fields['applied_for_in_indigenous_program'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['years_lived_in_region'].label = 'Prior to your entry to UPCM, how many years have you lived in that region?'
    
    class Meta:
        model = Survey_Data
        fields = ['applied_for_in_indigenous_program', 'is_accepted_in_UPCM_program', 'is_accepted_in_indigenous_program', 'years_lived_in_region', 'native_language_proficiency']


class Family_SurveyDataForm(forms.ModelForm):
    BIRTH_ORDER = (
            ('only child', 'Only Child'),
            ('youngest', 'Youngest'),
            ('eldest', 'Eldest'),
            ('others', 'Others')
        )

    def __init__(self, *args, **kwargs):
        super(Family_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['number_of_brothers'].label = 'Number of Brothers'
        self.fields['number_of_brothers'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
        self.fields['number_of_sisters'].label = 'Number of Sisters'
        self.fields['number_of_sisters'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
        self.fields['birth_order'] = forms.ChoiceField(choices=self.BIRTH_ORDER, widget=forms.RadioSelect())
        self.fields['birth_order_others'].label = 'If Others, specify'
        self.fields['birth_order_others'].widget = forms.TextInput(attrs={'class': 'short'})

        self.fields['number_of_second_degree_relatives_outside_PH'].label = 'How many?'
        self.fields['number_of_second_degree_relatives_outside_PH'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
        self.fields['relatives_residence_address'].label = 'Where was the residence of the majority of your relatives?'
        self.fields['relatives_residence_address'].widget = forms.TextInput(attrs={'class': 'address-fields', 'placeholder' : 'City or Town/Province/Country'})

        self.fields['have_second_degree_relatives_outside_PH'].label = 'At the time of your entry to UPCM, did you have any second degree relatives who were outside the Philippines?'
        self.fields['have_second_degree_relatives_outside_PH'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['have_second_degree_relatives_outside_PH'].required = True

        self.fields['have_health_profession_relatives'].label = 'At the time of your entry to UPCM, did you have any family members/relatives who were health professionals/practitioners?'
        self.fields['have_health_profession_relatives'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['have_health_profession_relatives'].required = True

        self.fields['have_relatives_with_stocks_in_PH'].label = 'At the time of your entry to UPCM, did you have any relatives with investments in hospitals who were residing in the Philippines'
        self.fields['have_relatives_with_stocks_in_PH'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['have_relatives_with_stocks_in_PH'].required = True

        self.fields['have_relatives_with_stocks_outside_PH'].label = 'At the time of your entry to UPCM, did you have any relatives with investments in hospitals who were residing outside the Philippines?'
        self.fields['have_relatives_with_stocks_outside_PH'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['have_relatives_with_stocks_outside_PH'].required = True

        self.fields['have_relatives_with_business'].label = 'At the time of your entry to UPCM, did you have  member/s of your family/relative who owned a business/es?'
        self.fields['have_relatives_with_business'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['have_relatives_with_business'].required = True

        self.fields['have_relatives_in_politics'].label = 'At the time of your entry to UPCM, did you or any member of your family/relative hold an office in the government?'
        self.fields['have_relatives_in_politics'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['have_relatives_in_politics'].required = True
    class Meta:
        model = Survey_Data
        fields = ['number_of_brothers', 'number_of_sisters', 'birth_order', 'birth_order_others',
        'have_second_degree_relatives_outside_PH', 'number_of_second_degree_relatives_outside_PH',
        'relatives_residence_address', 'have_health_profession_relatives', 'have_relatives_with_stocks_in_PH',
        'have_relatives_with_stocks_outside_PH', 'have_relatives_with_business', 'have_relatives_in_politics']


class GuardianForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(GuardianForm, self).__init__(*args, **kwargs)
        self.fields['relationship'].widget = forms.Select(attrs={'class' : 'short'}, choices=Guardian.RELATIONSHIP_CHOICES)

    class Meta:
        model = Guardian
        exclude = ['survey_data',]


class SiblingForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SiblingForm, self).__init__(*args, **kwargs)
        self.fields['age'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
        self.fields['birth_order'].widget = forms.NumberInput(attrs={'class': 'number-fields number'})
        self.fields['school_type'].widget = forms.Select(attrs={'class' : 'short'}, choices=Child_School.SCHOOL_TYPE_CHOICES)
        self.fields['name'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['school_or_occupation'].widget = forms.TextInput(attrs={'class': 'short'})
    class Meta:
        model = Sibling
        exclude = ['survey_data',]


class Relative_Outside_PHFOrm(forms.ModelForm):
    class Meta:
        model = Relative_Outside_PH
        exclude = ['survey_data']

class Relative_In_Health_ProfessionForm(forms.ModelForm):
    class Meta:
        model = Relative_In_Health_Profession
        exclude = ['survey_data']


class Relative_With_StocksForm(forms.ModelForm):
    class Meta:
        model = Relative_With_Stocks
        exclude = ['survey_data']

class Relative_With_BusinessForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(Relative_With_BusinessForm, self).__init__(*args, **kwargs)
        self.fields['business_size'].widget = forms.TextInput(attrs={'class': 'short'})

    class Meta:
        model = Relative_With_Business
        exclude = ['survey_data']


class Relative_In_PoliticsForm(forms.ModelForm):
    class Meta:
        model = Relative_In_Politics
        exclude = ['survey_data']


class Socio_Economic_SurveyDataForm(forms.ModelForm):
    SOURCES_OF_INCOME = (
            (1, 'Employment'),
            (2, 'Business'),
            (3, 'Others'),
        )

    PROPERTIES_OWNED = (
            (1, 'House'),
            (2, 'Condominium Unit'),
            (3, 'None'),
            (4, 'Residential/Agricultural Land'),
        )

    VEHICLES_OWNED = (
            (1, 'Motorcycle'),
            (2, 'Car'),
            (3, 'Van'),
            (4, 'None'),
            (5, 'Others'),
        )

    LEISURE_ACTIVITIES = (
            (1, 'Strolling or shopping at the mall'),
            (2, 'Fine dining, bar-hopping or disco'),
            (3, 'Watching concerts, Broadway or plays'),
            (4, 'Going to the gym or sports club'),
            (5, 'Swimming, strolling or picnic at the beach'),
            (6, 'Hiking, mountain climbing or nature trips'),
            (7, 'Gardening, cooking or home improvement'),
            (8, 'Staying at home and spending quality time with family'),
            (9, 'Others'),

        )


    def __init__(self, *args, **kwargs):
        super(Socio_Economic_SurveyDataForm, self).__init__(*args, **kwargs)
        self.fields['source_of_income'] = forms.MultipleChoiceField(choices=self.SOURCES_OF_INCOME, widget=forms.CheckboxSelectMultiple)
        self.fields['source_of_income'].label = "At the time of entry, what was your family's source of income?"
        self.fields['family_annual_income'] = forms.ChoiceField(choices=Survey_Data.FAMILIY_INCOME_RANGE_CHOICES, widget=forms.RadioSelect())
        self.fields['family_annual_income'].label = "At the time of entry, what is your best estimate of your family's annual income?"
        self.fields['frequency_of_travel'] = forms.ChoiceField(choices=Survey_Data.FREQUENCY_OF_TRAVEL_CHOICES, widget=forms.RadioSelect())
        self.fields['socio_economic_level'] = forms.ChoiceField(choices=Survey_Data.SOCIO_ECONOMIC_LEVEL_CHOICES, widget=forms.RadioSelect())
        self.fields['socio_economic_level'].label = 'At the time of entry, what is your socioeconomic level?'
        self.fields['source_of_income_others'].label = 'If Others, specify'

        self.fields['stfap_bracket'].label = 'At the time of entry, what was your enrollment/financial classification as a student?'
        self.fields['frequency_of_travel'].label = 'Prior to entering UPCM, how many trips  have you had outside the Philippines which were paid for by you or your family? '
        self.fields['stfap_bracket'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['properties_owned'] = forms.MultipleChoiceField(choices=self.PROPERTIES_OWNED, widget=forms.CheckboxSelectMultiple)
        #self.fields['properties_owned'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['properties_owned'].label = 'At the time of your entry, how many real estate properties did you and/or your family own?'

        self.fields['vehicles_owned'] = forms.MultipleChoiceField(choices=self.VEHICLES_OWNED, widget=forms.CheckboxSelectMultiple)
        #self.fields['vehicles_owned'].widget = forms.TextInput(attrs={'class': 'short'})
        self.fields['vehicles_owned'].label = 'At the time of your entry, how many motor vehicles did you and/or your family own?'

        self.fields['leisure_activities'] = forms.ModelMultipleChoiceField(queryset=Leisure.objects.all(),
                                          label='Preferred leisure activities (at the time of your entry to UPCM) Please rank the following leisure activities from 1-8 (1 being most preferred)',
                                          required=False,
                                          widget=FilteredSelectMultiple(
                                                    'leisure activities',
                                                    is_stacked=True,
                                                    attrs={'size':'10'}
                                          ))
    class Meta:
        model = Survey_Data
        fields = ['source_of_income', 'source_of_income_others', 'family_annual_income', 'socio_economic_level', 'stfap_bracket',
        'properties_owned', 'number_of_house', 'number_of_residential_land', 'number_of_condo', 'vehicles_owned', 'number_of_motorcycle', 'number_of_van',
        'number_of_car',  'vehicles_owned_others', 'number_of_others', 'frequency_of_travel', 'leisure_activities', 'leisure_activities_others']


class Rural_LifeForm(forms.ModelForm):
    REASONS_FOR_TAKING_MED = (
            ('family', 'Family'),
            ('lifestyle', 'Lifestyle'),
            ('professional advancement', 'Professional Advancement'),
            ('needs of the community', 'Needs of the community'),
            ('financial security', 'Financial Security'),
            ('service', 'Service'),
            ('safety', 'Safety'),
            ('others', 'Others'),
        )

    COMPASSION_RATING = (
            (1, 1),
            (2, 2),
            (3, 3),
            (4, 4),
            (5, 5),
            (6, 6),
            (7, 7),
            (8, 8),
            (9, 9),
            (10, 10),
        )

    APPROP_FOR_MED = (
            (1, 'GP'),
            (2, 'Family Medicine'),
            (3, 'Obstetrics-Gynecology'),
            (4, 'Internal Medicine'),
            (5, 'Others'),
        )

    def __init__(self, *args, **kwargs):
        super(Rural_LifeForm, self).__init__(*args, **kwargs)
        self.fields['reasons_for_taking_medicine'] = forms.MultipleChoiceField(choices=get_options(self.REASONS_FOR_TAKING_MED), widget=forms.CheckboxSelectMultiple)
        #self.fields['reasons_for_taking_medicine'].initial = [1, ]
        self.fields['reasons_for_taking_medicine'].label = 'At the time of your entry, what were your top three reasons for taking up medicine? '
        self.fields['reasons_for_taking_medicine_others'].label = 'If Others, specify'
        self.fields['compassion_rating'] = forms.ChoiceField(choices=self.COMPASSION_RATING, widget=forms.RadioSelect())
        self.fields['compassion_rating'].label = 'Rate your self-perception of being nationalistic, humanitarianistic, and compassionate for the poor on a scale of 1-10 (1 being the lowest and 10 being the highest)'
        self.fields['interest_in_rural_life'] = forms.ChoiceField(choices=Survey_Data.INTEREST_RATE_CHOICES, widget=forms.RadioSelect())
        self.fields['interest_in_rural_medical_practice'] = forms.ChoiceField(choices=Survey_Data.INTEREST_RATE_CHOICES, widget=forms.RadioSelect())
        self.fields['appropriate_specialty_for_rural_medical_practice'] = forms.MultipleChoiceField(choices=self.APPROP_FOR_MED, widget=forms.CheckboxSelectMultiple)
        self.fields['appropriate_specialty_for_rural_medical_practice'].label = 'What specialty did you think was the most appropriate for rural medical practice?'
        self.fields['appropriate_specialty_for_rural_medical_practice_others'].label = 'If Others, specify'
    class Meta:
        model = Survey_Data
        fields = ['reasons_for_taking_medicine', 'reasons_for_taking_medicine_others', 'compassion_rating', 'interest_in_rural_life', 'interest_in_rural_medical_practice',
        'appropriate_specialty_for_rural_medical_practice', 'appropriate_specialty_for_rural_medical_practice_others']


class PostGraduateTrainingForm(forms.ModelForm):
    class Meta:
        model = Survey_Data
        fields = ['have_received_scholarship', 'scholarship_classification', 'scholarship_classification_other','had_rural_training', 'rural_traning_type', 'rural_experience_description',
        'have_role_model_mentor', 'role_model_mentor_position', 'role_model_mentor_location',
        'internship_type', 'internship_department', 'internship_type_other', 'had_training_outside_PH',
        'did_rural_living_interest_change', 'rural_living_interest_change_reason', 'rural_living_interest_change_when',
        'did_rural_practice_interest_change', 'rural_practice_interest_change_reason', 'rural_practice_interest_change_when',
        'is_inclined_to_medical_practice', 'is_inclined_to_medical_practice_reason']
    def __init__(self, *args, **kwargs):
        super(PostGraduateTrainingForm, self).__init__(*args, **kwargs)
        self.fields['have_received_scholarship'].label = 'Did you receive any scholarship?'
        self.fields['scholarship_classification_other'].label = 'If Other, specify'
        self.fields['scholarship_classification'].label = 'Specify classification of scholarship'
        self.fields['have_received_scholarship'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['rural_living_interest_change_reason'].label = 'If yes, why?'
        self.fields['had_rural_training'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['had_rural_training'].label = 'Did you have any part of your medical school or postgraduate training in a rural location?'
        self.fields['rural_traning_type'].label = 'Specify type'
        self.fields['rural_experience_description'].label = 'Please describe the rural experience'
        self.fields['have_role_model_mentor'].label = 'Did you have a mentor/role model in the medical field (UPCM or outside UPCM) whom you greatly admired and who inspired you?'
        self.fields['have_role_model_mentor'].widget = forms.Select(attrs={'class' : 'short'}, choices=Survey_Data.YES_NO_CHOICES)
        self.fields['had_training_outside_PH'].label = 'Did you have any part of your medical school or postgraduate training outside the Philippines?'
        self.fields['role_model_mentor_position'].label = 'Specify your mentor\'s work/position'
        self.fields['role_model_mentor_location'].label = 'Specify your mentor\'s location'
        self.fields['internship_type'].label = 'Type of Internship'
        self.fields['internship_department'].label = 'Specify department'
        self.fields['internship_type_other'].label = 'If Other, specify'
        self.fields['did_rural_practice_interest_change'].label = 'Did your interest in living in a rural community change?'
        self.fields['rural_living_interest_change_reason'].label = 'If yes, why?'
        self.fields['rural_practice_interest_change_when'].label = 'Did your interest in doing  rural medical practice change?'
        self.fields['rural_practice_interest_change_reason'].label = 'If yes, why?'
        self.fields['is_inclined_to_medical_practice'].label = 'At the time of completing medical training at UPCM, did you feel ready or inclined to undertake a rural medical practice?'
        self.fields['is_inclined_to_medical_practice_reason'].label = 'If yes, why?'


class SuggestionForm(forms.ModelForm):
    class Meta:
        model = Survey_Data
        fields = ['suggestion', ]

    def __init__(self, *args, **kwargs):
        super(SuggestionForm, self).__init__(*args, **kwargs)
        self.fields['suggestion'].widget = forms.Textarea(attrs={'rows': 5, 'cols': 50, 'style': 'width: 425px'})
        self.fields['suggestion'].label = 'Comments / Suggestions:'

# Author: Rescian Rey
# Description: Sign up form
class SignUpForm(forms.Form):

    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder' : 'Email address', 
        'class':'form-control', 'required':'', 'autofocus':'', 'type':'email'}))
    mobile = forms.CharField(max_length=13, validators=[
        RegexValidator(
            regex='^\+\d{12}$',
            message='Mobile number should only consist numeric characters. Format should be +63XXXXXXXXXX.',
            code='invalid_mobile'
        )], widget=forms.TextInput(attrs={'placeholder' : '+63XXXXXXXXXX', 
        'class':'form-control', 'required':'', 'autofocus':'', 'type':'text'}))
    graduate_of_upcm = forms.BooleanField(initial=False, required=False)
    spent_years_as_physician = forms.BooleanField(initial=False, required=False)


    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()

        # Check for duplicates
        if cleaned_data.get('email', None) and cleaned_data.get('mobile', None):
            duplicates = Participant.objects.filter(email=cleaned_data['email'], mobile=cleaned_data['mobile'], active=True)
            if duplicates.exists():
                raise forms.ValidationError("Email and mobile number were already registered and active.")

    def clean_graduate_of_upcm(self):
        graduate_of_upcm = self.cleaned_data['graduate_of_upcm']

        if not graduate_of_upcm:
            raise forms.ValidationError("Confirm that you are a graduate of UPCM.")

        return graduate_of_upcm

    def clean_spent_years_as_physician(self):
        spent_years_as_physician = self.cleaned_data['spent_years_as_physician']

        if not spent_years_as_physician:
            raise forms.ValidationError("Confirm that you spent years as a physician.")

        return spent_years_as_physician


# Author: Rescian Rey
# Description: Login form
class LoginForm(forms.Form):
    passcode = forms.CharField(max_length=10,
        widget=forms.TextInput(
            attrs={'class':'form-control',
                    'placeholder':'Passcode',
                    'required':"",
                    'autofocus':'',
                    'type':'text',
            })
        )
    participant = None

    def clean_passcode(self):
        passcode = self.cleaned_data['passcode']

        participant_using_passcode = Participant.objects.filter(token=passcode)
        if not participant_using_passcode.exists():
            raise forms.ValidationError('Invalid passcode. Please ensure you typed in the correct passcode.')

        self.participant = participant_using_passcode[0]

        if not self.participant.active:
            activate_participant(self.participant)
        return passcode


# Lost Passcode Form
class LostPasscodeForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder' : 'Email address', 
        'class':'form-control', 'required':'', 'autofocus':'', 'type':'email'}))
    mobile = forms.CharField(max_length=13, validators=[
        RegexValidator(
            regex='^\+\d{12}$',
            message='Mobile number should only consist numeric characters. Format should be +63XXXXXXXXXX.',
            code='invalid_mobile'
        )], widget=forms.TextInput(attrs={'placeholder' : '+63XXXXXXXXXX', 
        'class':'form-control', 'required':'', 'autofocus':'', 'type':'text'}))

    def clean(self):
        cleaned_data = super(LostPasscodeForm, self).clean()

        if cleaned_data.get('email', None) and cleaned_data.get('mobile', None):
            participant = Participant.objects.filter(email=cleaned_data['email'], mobile=cleaned_data['mobile'])

            if not participant.exists():
                signup_url = reverse('signup')
                err_message = 'Email and mobile number are not yet registered. Sign up <a href="%s">here</a>.' % signup_url
                raise forms.ValidationError(err_message)

            self.participant = participant[0]



def get_options(opt_list):
    return tuple([(i, item[1]) for i, item in enumerate(opt_list)])

