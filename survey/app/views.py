from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.utils.decorators import method_decorator
from django.views.generic import View

from app.forms import *
from app.views_utils import create_formset, generate_passcode, login, send_sms, send_credentials, get_survey
from app.models import Participant

from survey import settings


# Author: Rescian Rey
# Description: Ensures user is logged in to view page
class LoginRequiredMixin(object):
    u"""Ensures that user must be authenticated in order to access view."""

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


# Author: Rescian Rey
# Description: Redirects user to success page if user has completed the survey.
class SurveyCompletedMixin(object):
    u"""Redirects user to success page if user has completed the survey."""

    def dispatch(self, request, *args, **kwargs):
        if hasattr(request.user, 'participant'):
            if request.user.participant.has_submitted:
                return redirect('completed')
        else:
             return redirect('signup')

        return super(SurveyCompletedMixin, self).dispatch(request, *args, **kwargs)


class SurveyPage(LoginRequiredMixin, SurveyCompletedMixin, View):

    template = ''
    context = {
        'category': '',
        'subcategory': '',
        'section_name':'',
    }

    form_class = None
    formset_list = [
            #('form', 'help text', 'related name to survey'),
        ]

    def get(self, request, *args, **kwargs):
        survey_data = get_survey(request)
        form = self.form_class(instance=survey_data)
        formsets = {}
        success = False
        errors = False

        for item in self.formset_list:
            formset_class = create_formset(item[0], item[1])
            formset = formset_class(queryset=getattr(survey_data, item[2]).all(), prefix=item[2])
            formsets[item[2]] = formset

        self.context.update({
            'form': form,
            'formsets': formsets,
            'success': success,
            'errors': errors,
        })

        return render_to_response(self.template, self.context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        redirect_url = None
        errors = False
        success = False
        survey_data = get_survey(request)
        form = self.form_class(request.POST, instance=survey_data)
        formsets = {}
        for item in self.formset_list:
            formset_class = create_formset(item[0], item[1])
            formset = formset_class(request.POST, prefix=item[2])
            formsets[item[2]] = formset

        is_valid = form.is_valid()
        formset_val = formsets.values()
        for formset in formset_val:
            is_valid = is_valid and formset.is_valid()

        if is_valid:
            form.save()
            for formset in formset_val:
                for rec in formset.save(commit=False):
                    rec.survey_data = survey_data
                formset.save()

            redirect_url = request.GET.get('next', None)
            success = True
        else:
            errors = True

        self.context.update({
            'form': form,
            'formsets': formsets,
            'errors': errors,
            'success': success,
        })

        response = None
        if redirect_url:
            response = redirect(redirect_url)
        else:
            response = render_to_response(self.template, self.context, context_instance=RequestContext(request))
        return response


class PersonalPage(SurveyPage):

    template = 'app/personal.html'
    context = {
        'category': 'current',
        'subcategory': 'personal',
        'section_name':'Personal Information',
    }

    form_class = Personal_SurveyDataForm
    formset_list = [
            #('form', 'help text', 'related name to survey', 'prefix'),
            (Child_SchoolForm, 'Children Information', 'children')
        ]


# Author: Rescian Rey
# Description: Occupation, training and practice page controller
class OccupationPage(SurveyPage):
    template = 'app/occupation.html'
    context = {
        'category': 'current',
        'subcategory': 'occupation',
        'section_name':'Medical Training, Practice or Other Occupation',
    }

    form_class = Occupation_SurveyDataForm
    formset_list = [
            #('form', 'help text', 'related name to survey'),
            (TrainingForm, 'Medical Training', 'trainings', ),
            (OccupationForm, 'Occupation (Please list all)', 'occupations',)
        ]


# Author: Rescian Rey
# Description: Perceptions on Influence page controller
class PerceptionsPage(SurveyPage):
    template = 'app/perceptions.html'
    context = {
        'category': 'current',
        'subcategory': 'perceptions',
        'section_name':'Perceptions on Influences on Current Medical Practice',
    }

    form_class = Perceptions_SurveyDataForm


# Author: Rescian Rey
# Description: Education background page controller
class EducationPage(SurveyPage):
    template = 'app/education.html'
    context = {
        'category': 'background',
        'subcategory': 'education',
        'section_name':'Personal and Educational Background',
    }

    form_class = Education_SurveyDataForm
    formset_list = [
            #('form', 'help text', 'related name to survey'),
            (EducationForm, 'Education', 'education', ),
        ]


# Author: Rescian Rey
# Description: Plans for Medical Practice page controller
class PlansPage(SurveyPage):

    template = 'app/plans.html'
    context = {
        'category': 'background',
        'subcategory': 'plans',
        'section_name':'Plans for Medical Practice',
    }

    form_class = Plans_SurveyDataForm


# Author: Rescian Rey
# Description: Region of Origin page controller
class Region_OriginPage(SurveyPage):

    template = 'app/region_origin.html'
    context = {
        'category': 'background',
        'subcategory':'region-origin',
        'section_name':'Region of Origin',
    }

    form_class = Region_Origin_SurveyDataForm


# Author: Rescian Rey
# Description: Family page controller
class FamilyPage(SurveyPage):
    template = 'app/family.html'
    context = {
        'category': 'family',
        'subcategory':'family',
        'section_name':'Family Information',
    }

    form_class = Family_SurveyDataForm
    formset_list = [
        (GuardianForm, 'Parents/Guardians ', 'guardians'),
        (SiblingForm, 'Siblings', 'siblings'),
        (Relative_Outside_PHFOrm, 'Relatives outside PH', 'relatives_outside_ph'),
        (Relative_In_Health_ProfessionForm, 'Relative in Health Profession', 'relatives_in_health_profession'),
        (Relative_With_StocksForm, 'Relative With Investments', 'relatives_with_stocks'),
        (Relative_With_BusinessForm, 'Relative With Business', 'relatives_with_business'),
        (Relative_In_PoliticsForm, 'Relative in Politics', 'relatives_in_politics'),
    ]


# Author: Rescian Rey
# Description: Socio Economic page controller
class Socio_EconomicPage(SurveyPage):

    template = 'app/socio_economic.html'
    context = {
        'category': 'socio-economic',
        'subcategory':'socio-economic',
        'section_name':'Socio Economic Status',
    }

    form_class = Socio_Economic_SurveyDataForm


# Author: Rescian Rey
# Description: Rural Life and Practice page controller
class Rural_LifePage(SurveyPage):
    template = 'app/rural_life.html'
    context = {
        'category': 'rural-life',
        'subcategory':'rural-life',
        'section_name':'Rural Life and Practice',
    }

    form_class = Rural_LifeForm


# Author: Rescian Rey
# Description: Post Graduate Training page controller
class PostGraduateTrainingPage(SurveyPage):
    template = 'app/post_graduate.html'
    context = {
        'category': 'post-graduate',
        'subcategory':'post-graduate',
        'section_name':'Rural Exposure',
    }

    form_class = PostGraduateTrainingForm

# Author: Rescian Rey
# Description: Summary page controller
class SubmissionPage(LoginRequiredMixin, View):
    template = 'app/submission.html'
    context = {
            'category': 'submission',
            'subcategory':'submission',
            'section_name':'Submission',
        }

    def get(self, request, *args, **kwargs):
        survey_data = get_survey(request)
        self.context['form'] = SuggestionForm(instance=survey_data)
        return render_to_response(self.template, self.context, context_instance=RequestContext(request))


    def post(self, request, *args, **kwargs):
        survey_data = get_survey(request)

        form = SuggestionForm(request.POST, instance=survey_data)
        if form.is_valid():
            form.save()
        # Mark the participant as has_submitted
        survey_data.participant.has_submitted = True
        survey_data.participant.save()

        survey_data.participant = None
        survey_data.save()

        return redirect('completed')




# Author: Rescian Rey
# Description: Sign Up page
class SignUp(View):
    form_class = SignUpForm
    template_name = 'app/signup.html'
    success_page = 'app/registration_success.html'



    # GET request
    def get(self, request, *args, **kwargs):
        context = {}
        form = self.form_class()

        context.update({'form': form})
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))

    # POST request
    def post(self, request, *args, **kwargs):
        context = {}
        form = self.form_class(request.POST)

        if form.is_valid():
            participant, created = Participant.objects.get_or_create(
                    email=form.cleaned_data['email'],
                    mobile=form.cleaned_data['mobile'],
                    defaults={'token': generate_passcode()}
                )
            send_credentials(participant)
            
            return render_to_response(self.success_page, context, context_instance=RequestContext(request))
        else:
            pass
        context.update({'form': form})
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


# Author: Rescian Rey
# Description: Sign Up page
class Login(View):
    template_name = 'app/login.html'
    form_class = LoginForm

    # GET request
    def get(self, request, *args, **kwargs):
        context = {}
        form = self.form_class()

        context.update({'form': form})
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


    # POST
    def post(self, request, *args, **kwargs):
        context = {}
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.participant.user;
            login(request, user)
            return redirect(reverse('personal'))
        else:
            print form.errors

        context.update({'form': form})
        return render_to_response(self.template_name, context, context_instance=RequestContext(request))


# Author: Rescian Rey
# Description: Lost Passcode page
class LostPasscode(View):
    template_name = 'app/lost_passcode.html'
    form_class = LostPasscodeForm
    success_page = 'app/registration_success.html'

    # GET request
    def get(self, request, *args, **kwargs):
        context = {}
        form = self.form_class()

        context.update({'form': form})

        return render_to_response(self.template_name, context, context_instance=RequestContext(request))

    # POST
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        context = {}

        if form.is_valid():
            participant = form.participant
            send_credentials(participant, first_time=False)

            return render_to_response(self.success_page, context, context_instance=RequestContext(request))
        else:
            print form.errors

        context = {
            'form': form,
        }

        return render_to_response(self.template_name, context, context_instance=RequestContext(request))