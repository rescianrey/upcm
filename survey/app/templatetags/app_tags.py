from django import template
from django.template.defaultfilters import stringfilter

from survey import settings

register = template.Library()

@register.filter
@stringfilter
def app_config(key):
	value = ''
	if key in settings.APP_CONFIG:
		value = settings.APP_CONFIG[key]
	return value