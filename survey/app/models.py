# Author: Rescian Rey
# Description: Models for the Survey App
# History:
#     10.29.2014    Rescian Rey        Created    

from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User

from multiselectfield import MultiSelectField

# CONVENTIONS:
# max length for addresses is 200
# max length for full name is 50


# Author: Rescian Rey
# Description: Contains majority of the participants data
# History:
#     10.29.2014    Rescian Rey        Created
class Survey_Data(models.Model):
    YES_NO_CHOICES = ((None, '-'), ('yes', 'Yes'), ('no', 'No'),)
    SEX_CHOICES = (('M', 'Male'), ('F', 'Female'))
    CITIZENSHIP_CHOICES = (
            ('Filipino', 'Filipino'),
            ('Other', 'Other'),
            ('Dual', 'Dual')
        )
    CIVIL_STATUS_CHOICES = (('S', 'Single'), ('M', 'Married'), ('SP', 'Separated'), ('W', 'Widow/er'))
    PLANNED_PRACTICE_LOCATION_CHOICES = (
            ('PH urban', 'Philippines (urban)'), 
            ('PH rural', 'Philippines (rural'), 
            ('outside PH', 'Outside the Philippines')
        )
    YEARS_LIVED_IN_REGION_RANGE = (
            ('0', '0'),
            ('1-3', '1-3'), 
            ('4-6', '4-6'), 
            ('7-10', '7-10'),
            ('>10', '>10'), 
        )
    FAMILIY_INCOME_RANGE_CHOICES = (
            ('>3,000,000', '>3,000,000'),
            ('1,000,001-3,000,000', '1,000,001-3,000,000'),
            ('500,001-1,000,000', '500,001-1,000,000'),
            ('250,001-500,000', '250,001-500,000'),
            ('135,001-250,000', '135,001-250,000'),
            ('>Poverty threshold-135,000', '>Poverty threshold-135,000'),
            ('0-Poverty threshold', '0-Poverty threshold'),
        )
    SOCIO_ECONOMIC_LEVEL_CHOICES = (
            ('Low', 'Low (PHP62,000)'),
            ('Low Middle', 'Low Middle (PHP191,000)'),
            ('Middle', 'Middle (Middle PHP603,000)'),
            ('Upper Middle/High', 'Upper Middle/High (PHP 1,857,000)'),
        )
    FREQUENCY_OF_TRAVEL_CHOICES = (
            ('0', '0'),
            ('1-3', '1-3'),
            ('4-6', '4-6'),
            ('7-10', '7-10'),
            ('>10', '>10'),
        )
    INTEREST_RATE_CHOICES = (
            (1, 'Very Weak'),
            (2, 'Weak'),
            (3, 'Moderate'),
            (4, 'Strong'),
            (5, 'Very Strong'),
        )
    RURAL_TRAINING_TYPE_CHOICES = (
            ('Required', 'Required course'),
            ('Elective', 'Elective course'),
            ('Extracurricular', 'Extracurricular')
        )
    INTEREST_CHANGE_WHEN_CHOICES = (
            ('during training', 'During training'),
            ('after training', 'After training')
        )
    YEAR_GRADUATED_CHOICES = (
        ('1990', '1990'),
        ('1991', '1991'),
        ('1992', '1992'),
        ('1993', '1993'),
        ('1994', '1994'),
        ('1995', '1995'),
        ('1996', '1996'),
        ('1997', '1997'),
        ('1998', '1998'),
        ('1999', '1999'),
        ('2000', '2000'),
        ('2001', '2001'),
        ('2002', '2002'),
        ('2003', '2003'),
        ('2004', '2004'),
        ('2005', '2005'),
        ('2006', '2006'),
        ('2007', '2007'),
        ('2008', '2008'),
        ('2009', '2009'),
        ('2010', '2010'),
    )

    COUNTRIES = (
        ('Aland Islands (Finland)', 'Aland Islands (Finland)'),
        ('Afghanistan', 'Afghanistan'),
        ('Albania', 'Albania'),
        ('Algeria', 'Algeria'),
        ('American Samoa (USA)', 'American Samoa (USA)'),
        ('Andorra', 'Andorra'),
        ('Angola', 'Angola'),
        ('Anguilla (UK)', 'Anguilla (UK)'),
        ('Antigua and Barbuda', 'Antigua and Barbuda'),
        ('Argentina', 'Argentina'),
        ('Armenia', 'Armenia'),
        ('Aruba (Netherlands)', 'Aruba (Netherlands)'),
        ('Australia', 'Australia'),
        ('Austria', 'Austria'),
        ('Azerbaijan', 'Azerbaijan'),
        ('Bahamas', 'Bahamas'),
        ('Bahrain', 'Bahrain'),
        ('Bangladesh', 'Bangladesh'),
        ('Barbados', 'Barbados'),
        ('Belarus', 'Belarus'),
        ('Belgium', 'Belgium'),
        ('Belize', 'Belize'),
        ('Benin', 'Benin'),
        ('Bermuda (UK)', 'Bermuda (UK)'),
        ('Bhutan', 'Bhutan'),
        ('Bolivia', 'Bolivia'),
        ('Bosnia and Herzegovina', 'Bosnia and Herzegovina'),
        ('Botswana', 'Botswana'),
        ('Brazil', 'Brazil'),
        ('British Virgin Islands (UK)', 'British Virgin Islands (UK)'),
        ('Brunei', 'Brunei'),
        ('Bulgaria', 'Bulgaria'),
        ('Burkina Faso', 'Burkina Faso'),
        ('Burma', 'Burma'),
        ('Burundi', 'Burundi'),
        ('Cambodia', 'Cambodia'),
        ('Cameroon', 'Cameroon'),
        ('Canada', 'Canada'),
        ('Cape Verde', 'Cape Verde'),
        ('Caribbean Netherlands (Netherlands)', 'Caribbean Netherlands (Netherlands)'),
        ('Cayman Islands (UK)', 'Cayman Islands (UK)'),
        ('Central African Republic', 'Central African Republic'),
        ('Chad', 'Chad'),
        ('Chile', 'Chile'),
        ('China', 'China'),
        ('Christmas Island (Australia)', 'Christmas Island (Australia)'),
        ('Cocos (Keeling) Islands (Australia)', 'Cocos (Keeling) Islands (Australia)'),
        ('Colombia', 'Colombia'),
        ('Comoros', 'Comoros'),
        ('Cook Islands (NZ)', 'Cook Islands (NZ)'),
        ('Costa Rica', 'Costa Rica'),
        ('Croatia', 'Croatia'),
        ('Cuba', 'Cuba'),
        ('Curacao (Netherlands)', 'Curacao (Netherlands)'),
        ('Cyprus', 'Cyprus'),
        ('Czech Republic', 'Czech Republic'),
        ('Democratic Republic of the Congo', 'Democratic Republic of the Congo'),
        ('Denmark', 'Denmark'),
        ('Djibouti', 'Djibouti'),
        ('Dominica', 'Dominica'),
        ('Dominican Republic', 'Dominican Republic'),
        ('Ecuador', 'Ecuador'),
        ('Egypt', 'Egypt'),
        ('El Salvador', 'El Salvador'),
        ('Equatorial Guinea', 'Equatorial Guinea'),
        ('Eritrea', 'Eritrea'),
        ('Estonia', 'Estonia'),
        ('Ethiopia', 'Ethiopia'),
        ('Falkland Islands (UK)', 'Falkland Islands (UK)'),
        ('Faroe Islands (Denmark)', 'Faroe Islands (Denmark)'),
        ('Federated States of Micronesia', 'Federated States of Micronesia'),
        ('Fiji', 'Fiji'),
        ('Finland', 'Finland'),
        ('France', 'France'),
        ('French Guiana (France)', 'French Guiana (France)'),
        ('French Polynesia (France)', 'French Polynesia (France)'),
        ('Gabon', 'Gabon'),
        ('Gambia', 'Gambia'),
        ('Georgia', 'Georgia'),
        ('Germany', 'Germany'),
        ('Ghana', 'Ghana'),
        ('Gibraltar (UK)', 'Gibraltar (UK)'),
        ('Greece', 'Greece'),
        ('Greenland (Denmark)', 'Greenland (Denmark)'),
        ('Grenada', 'Grenada'),
        ('Guadeloupe (France)', 'Guadeloupe (France)'),
        ('Guam (USA)', 'Guam (USA)'),
        ('Guatemala', 'Guatemala'),
        ('Guernsey (UK)', 'Guernsey (UK)'),
        ('Guinea', 'Guinea'),
        ('Guinea-Bissau', 'Guinea-Bissau'),
        ('Guyana', 'Guyana'),
        ('Haiti', 'Haiti'),
        ('Honduras', 'Honduras'),
        ('Hong Kong (China)', 'Hong Kong (China)'),
        ('Hungary', 'Hungary'),
        ('Iceland', 'Iceland'),
        ('India', 'India'),
        ('Indonesia', 'Indonesia'),
        ('Iran', 'Iran'),
        ('Iraq', 'Iraq'),
        ('Ireland', 'Ireland'),
        ('Isle of Man (UK)', 'Isle of Man (UK)'),
        ('Israel', 'Israel'),
        ('Italy', 'Italy'),
        ('Ivory Coast', 'Ivory Coast'),
        ('Jamaica', 'Jamaica'),
        ('Japan', 'Japan'),
        ('Jersey (UK)', 'Jersey (UK)'),
        ('Jordan', 'Jordan'),
        ('Kazakhstan', 'Kazakhstan'),
        ('Kenya', 'Kenya'),
        ('Kiribati', 'Kiribati'),
        ('Kosovo', 'Kosovo'),
        ('Kuwait', 'Kuwait'),
        ('Kyrgyzstan', 'Kyrgyzstan'),
        ('Laos', 'Laos'),
        ('Latvia', 'Latvia'),
        ('Lebanon', 'Lebanon'),
        ('Lesotho', 'Lesotho'),
        ('Liberia', 'Liberia'),
        ('Libya', 'Libya'),
        ('Liechtenstein', 'Liechtenstein'),
        ('Lithuania', 'Lithuania'),
        ('Luxembourg', 'Luxembourg'),
        ('Macau (China)', 'Macau (China)'),
        ('Macedonia', 'Macedonia'),
        ('Madagascar', 'Madagascar'),
        ('Malawi', 'Malawi'),
        ('Malaysia', 'Malaysia'),
        ('Maldives', 'Maldives'),
        ('Mali', 'Mali'),
        ('Malta', 'Malta'),
        ('Marshall Islands', 'Marshall Islands'),
        ('Martinique (France)', 'Martinique (France)'),
        ('Mauritania', 'Mauritania'),
        ('Mauritius', 'Mauritius'),
        ('Mayotte (France)', 'Mayotte (France)'),
        ('Mexico', 'Mexico'),
        ('Moldov', 'Moldov'),
        ('Monaco', 'Monaco'),
        ('Mongolia', 'Mongolia'),
        ('Montenegro', 'Montenegro'),
        ('Montserrat (UK)', 'Montserrat (UK)'),
        ('Morocco', 'Morocco'),
        ('Mozambique', 'Mozambique'),
        ('Namibia', 'Namibia'),
        ('Nauru', 'Nauru'),
        ('Nepal', 'Nepal'),
        ('Netherlands', 'Netherlands'),
        ('New Caledonia (France)', 'New Caledonia (France)'),
        ('New Zealand', 'New Zealand'),
        ('Nicaragua', 'Nicaragua'),
        ('Niger', 'Niger'),
        ('Nigeria', 'Nigeria'),
        ('Niue (NZ)', 'Niue (NZ)'),
        ('Norfolk Island (Australia)', 'Norfolk Island (Australia)'),
        ('North Korea', 'North Korea'),
        ('Northern Mariana Islands (USA)', 'Northern Mariana Islands (USA)'),
        ('Norway', 'Norway'),
        ('Oman', 'Oman'),
        ('Pakistan', 'Pakistan'),
        ('Palau', 'Palau'),
        ('Palestine', 'Palestine'),
        ('Panama', 'Panama'),
        ('Papua New Guinea', 'Papua New Guinea'),
        ('Paraguay', 'Paraguay'),
        ('Peru', 'Peru'),
        ('Philippines', 'Philippines'),
        ('Pitcairn Islands (UK)', 'Pitcairn Islands (UK)'),
        ('Poland', 'Poland'),
        ('Portugal', 'Portugal'),
        ('Puerto Rico', 'Puerto Rico'),
        ('Qatar', 'Qatar'),
        ('Republic of the Congo', 'Republic of the Congo'),
        ('Reunion (France)', 'Reunion (France)'),
        ('Romania', 'Romania'),
        ('Russia', 'Russia'),
        ('Rwanda', 'Rwanda'),
        ('Saint Barthelemy (France)', 'Saint Barthelemy (France)'),
        ('Saint Helena, Ascension and Tristan da Cunha (UK)', 'Saint Helena, Ascension and Tristan da Cunha (UK)'),
        ('Saint Kitts and Nevis', 'Saint Kitts and Nevis'),
        ('Saint Lucia', 'Saint Lucia'),
        ('Saint Martin (France)', 'Saint Martin (France)'),
        ('Saint Pierre and Miquelon (France)', 'Saint Pierre and Miquelon (France)'),
        ('Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines'),
        ('Samoa', 'Samoa'),
        ('San Marino', 'San Marino'),
        ('Sao Tom and Principe', 'Sao Tom and Principe'),
        ('Saudi Arabia', 'Saudi Arabia'),
        ('Senegal', 'Senegal'),
        ('Serbia', 'Serbia'),
        ('Seychelles', 'Seychelles'),
        ('Sierra Leone', 'Sierra Leone'),
        ('Singapore', 'Singapore'),
        ('Sint Maarten (Netherlands)', 'Sint Maarten (Netherlands)'),
        ('Slovakia', 'Slovakia'),
        ('Slovenia', 'Slovenia'),
        ('Solomon Islands', 'Solomon Islands'),
        ('Somalia', 'Somalia'),
        ('South Africa', 'South Africa'),
        ('South Korea', 'South Korea'),
        ('South Sudan', 'South Sudan'),
        ('Spain', 'Spain'),
        ('Sri Lanka', 'Sri Lanka'),
        ('Sudan', 'Sudan'),
        ('Suriname', 'Suriname'),
        ('Svalbard and Jan Mayen (Norway)', 'Svalbard and Jan Mayen (Norway)'),
        ('Swaziland', 'Swaziland'),
        ('Sweden', 'Sweden'),
        ('Switzerland', 'Switzerland'),
        ('Syria', 'Syria'),
        ('Taiwan', 'Taiwan'),
        ('Tajikistan', 'Tajikistan'),
        ('Tanzania', 'Tanzania'),
        ('Thailand', 'Thailand'),
        ('Timor-Leste', 'Timor-Leste'),
        ('Togo', 'Togo'),
        ('Tokelau (NZ)', 'Tokelau (NZ)'),
        ('Tonga', 'Tonga'),
        ('Trinidad and Tobago', 'Trinidad and Tobago'),
        ('Tunisia', 'Tunisia'),
        ('Turkey', 'Turkey'),
        ('Turkmenistan', 'Turkmenistan'),
        ('Turks and Caicos Islands (UK)', 'Turks and Caicos Islands (UK)'),
        ('Tuvalu', 'Tuvalu'),
        ('Uganda', 'Uganda'),
        ('Ukraine', 'Ukraine'),
        ('United Arab Emirates', 'United Arab Emirates'),
        ('United Kingdom', 'United Kingdom'),
        ('United States', 'United States'),
        ('United States Virgin Islands (USA)', 'United States Virgin Islands (USA)'),
        ('Uruguay', 'Uruguay'),
        ('Uzbekistan', 'Uzbekistan'),
        ('Vanuatu', 'Vanuatu'),
        ('Vatican City', 'Vatican City'),
        ('Venezuela', 'Venezuela'),
        ('Vietnam', 'Vietnam'),
        ('Wallis and Futuna (France)', 'Wallis and Futuna (France)'),
        ('Western Sahara', 'Western Sahara'),
        ('Yemen', 'Yemen'),
        ('Zambia', 'Zambia'),
        ('Zimbabwe', 'Zimbabwe')
    )

    SPECIALIZATION = (
        ('Administration','Administration'),
        ('Anesthesiology','Anesthesiology'),
        ('Community Medicine','Community Medicine'),
        ('Dermatology','Dermatology'),
        ('Emergency Medicine','Emergency Medicine'),
        ('Epidemiology','Epidemiology'),
        ('Family Medicine','Family Medicine'),
        ('Internal Med','Internal Med'),
        ('Neurology','Neurology'),
        ('Nuclear Med','Nuclear Med'),
        ('OB/Gyne','OB/Gyne'),
        ('Ophthalmology','Ophthalmology'),
        ('Orthopedics','Orthopedics'),
        ('Otorhinolaryngology','Otorhinolaryngology'),
        ('Pathology','Pathology'),
        ('Pediatrics','Pediatrics'),
        ('Psychiatry','Psychiatry'),
        ('Public Health','Public Health'),
        ('Radiology','Radiology'),
        ('Rehabilitation','Rehabilitation'),
        ('Surgery','Surgery'),
        ('Academe/Teaching','Academe/Teaching'),
        ('Research','Research'),
    )
    IMMIGRANT_STATUS = (
        ("petitioned (in process)", "petitioned (in process)"),
        ("approved", "approved"),
        ("denied", "denied")
    )

    SCHOLARSHIIP_CLASSIFICATION = (
            ('academic', 'Academic'),
            ('athletic', 'Non-Academic, Athletic'),
            ('cultural', 'Non-Academic, Cultural'),
            ('other', 'Non-Academic, Other'),
        )

    INTERNSHIP_TYPES = (
            ('straight', 'Straight Internship'),
            ('rotating', 'Rotating Internship'),
            ('other', 'Other'),
        )
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    birth_place = models.CharField(max_length=200, null=True, blank=True)
    citizenship = models.CharField(choices=CITIZENSHIP_CHOICES, max_length=8, null=True, blank=True)
    citizenship_desc = models.CharField(max_length=50, null=True, blank=True)
    current_address = models.CharField(max_length=200, null=True, blank=True)
    permanent_address = models.CharField(max_length=200, null=True, blank=True)
    civil_status = models.CharField(max_length=2, choices=CIVIL_STATUS_CHOICES, null=True, blank=True)
    number_of_children = models.IntegerField(null=True, blank=True)
    year_of_marriage =  models.CharField(max_length=4, validators=[
        RegexValidator(
            regex='^\d{4}$',
            message='Year must be numeric.',
            code='invalid_year'
        )], null=True, blank=True)
    spouse_occupation = models.CharField(max_length=50, null=True, blank=True)
    spouse_occupation_location = models.CharField(max_length=200, null=True, blank=True)
    year_graduated = models.CharField(max_length=4, choices=YEAR_GRADUATED_CHOICES,validators=[
        RegexValidator(
            regex='^\d{4}$',
            message='Year must be numeric.',
            code='invalid_year'
        )], null=True, blank=True)
    board_certified = models.BooleanField(default=False)
    is_primary_occupation_medical = models.CharField(max_length=3, null=True, blank=True, choices=YES_NO_CHOICES)
    primary_occupation_specialization = models.CharField(max_length=100, choices=SPECIALIZATION, null=True, blank=True)
    primary_occupation_nature = models.CharField(max_length=500, null=True, blank=True)
    primary_occupation_nature_others = models.CharField(max_length=100, null=True, blank=True)
    non_medical_occupation_nature = models.CharField(max_length=200, null=True, blank=True)
    non_medical_occupation_employer = models.CharField(max_length=100, null=True, blank=True)
    non_medical_occupation_location = models.CharField(max_length=200, null=True, blank=True)
    physician_practice_location_influence = models.CharField(max_length=200, null=True, blank=True)
    physician_practice_location_influence_others = models.CharField(max_length=100, null=True, blank=True)
    participant_practice_location_influence = models.CharField(max_length=200, null=True, blank=True)
    participant_practice_location_influence_others = models.CharField(max_length=100, null=True, blank=True)
    civil_status_at_entry = models.CharField(max_length=2, choices=CIVIL_STATUS_CHOICES, null=True, blank=True)
    is_in_a_relationship = models.CharField(max_length=3, choices=YES_NO_CHOICES, null=True, blank=True)
    person_in_relationship_with_address = models.CharField(max_length=200, null=True, blank=True)
    spouse_origin_address = models.CharField(max_length=200, null=True, blank=True)
    is_petitioned = models.CharField(max_length=3, choices=YES_NO_CHOICES, null=True, blank=True)
    immigrant_country = models.CharField(max_length=100, choices=COUNTRIES, null=True, blank=True)
    immigrant_status = models.CharField(max_length=23, choices=IMMIGRANT_STATUS, null=True, blank=True) 
    is_volunteer = models.CharField(max_length=3, choices=YES_NO_CHOICES, null=True, blank=True)
    volunteer_work_nature = models.CharField(max_length=200, null=True, blank=True)
    volunteer_work_nature_others = models.CharField(max_length=100, null=True, blank=True)
    volunteer_work_roles = models.CharField(max_length=200, null=True, blank=True)
    volunteer_work_roles_others = models.CharField(max_length=100, null=True, blank=True)
    volunteer_work_location = models.CharField(max_length=200, null=True, blank=True)
    volunteer_work_location_others = models.CharField(max_length=100, null=True, blank=True)
    planned_practice_location = models.CharField(max_length=10, choices=PLANNED_PRACTICE_LOCATION_CHOICES, null=True, blank=True)
    specialties = models.ManyToManyField('Specialty')
    other_specialty = models.CharField(max_length=100, null=True, blank=True)
    applied_for_in_indigenous_program = models.CharField(max_length=10, choices=YES_NO_CHOICES, null=True, blank=True)
    is_accepted_in_UPCM_program = models.BooleanField(default=False)
    is_accepted_in_indigenous_program = models.BooleanField(default=False)
    years_lived_in_region = models.CharField(max_length=5, choices=YEARS_LIVED_IN_REGION_RANGE, null=True, blank=True)
    native_language_proficiency = models.CharField(max_length=200, null=True, blank=True)
    number_of_brothers = models.IntegerField(null=True, blank=True)
    number_of_sisters = models.IntegerField(null=True, blank=True)
    birth_order = models.CharField(max_length=50, null=True, blank=True)
    birth_order_others = models.CharField(max_length=100, null=True, blank=True)
    have_second_degree_relatives_outside_PH = models.CharField(max_length=3, null=True, blank=True)
    number_of_second_degree_relatives_outside_PH = models.IntegerField(null=True, blank=True)
    relatives_residence_address = models.CharField(max_length=200, null=True, blank=True)
    have_health_profession_relatives = models.CharField(max_length=3, null=True, blank=True)
    have_relatives_with_stocks_in_PH = models.CharField(max_length=3, null=True, blank=True)
    have_relatives_with_stocks_outside_PH = models.CharField(max_length=3, null=True, blank=True)
    have_relatives_with_business = models.CharField(max_length=3, null=True, blank=True)
    have_relatives_in_politics = models.CharField(max_length=3, null=True, blank=True)
    source_of_income = models.CharField(max_length=100, null=True, blank=True)
    source_of_income_others = models.CharField(max_length=100, null=True, blank=True)
    family_annual_income = models.CharField(max_length=30, choices=FAMILIY_INCOME_RANGE_CHOICES, null=True, blank=True)
    socio_economic_level = models.CharField(max_length=20, choices=SOCIO_ECONOMIC_LEVEL_CHOICES, null=True, blank=True)
    stfap_bracket = models.CharField(max_length=2, null=True, blank=True)
    properties_owned = models.CharField(max_length=100, null=True, blank=True)
    number_of_house = models.IntegerField(null=True, blank=True)
    number_of_residential_land = models.IntegerField(null=True, blank=True)
    number_of_condo = models.IntegerField(null=True, blank=True)
    number_of_motorcycle = models.IntegerField(null=True, blank=True)
    number_of_van = models.IntegerField(null=True, blank=True)
    number_of_car = models.IntegerField(null=True, blank=True)
    number_of_others = models.IntegerField(null=True, blank=True)
    vehicles_owned = models.CharField(max_length=100, null=True, blank=True)
    vehicles_owned_others = models.CharField(max_length=100, null=True, blank=True)
    frequency_of_travel = models.CharField(max_length=5, choices=FREQUENCY_OF_TRAVEL_CHOICES, null=True, blank=True)
    leisure_activities = models.ManyToManyField('Leisure')
    leisure_activities_others = models.CharField(max_length=100, null=True, blank=True)
    reasons_for_taking_medicine = models.CharField(max_length=200, null=True, blank=True)
    reasons_for_taking_medicine_others = models.CharField(max_length=100, null=True, blank=True)
    compassion_rating = models.IntegerField(null=True, blank=True)
    interest_in_rural_life = models.IntegerField(choices=INTEREST_RATE_CHOICES, null=True, blank=True)
    interest_in_rural_medical_practice = models.IntegerField(choices=INTEREST_RATE_CHOICES, null=True, blank=True)
    appropriate_specialty_for_rural_medical_practice = models.CharField(max_length=100, null=True, blank=True)
    appropriate_specialty_for_rural_medical_practice_others = models.CharField(max_length=100, null=True, blank=True)
    have_received_scholarship = models.CharField(max_length=3, null=True, blank=True, choices=YES_NO_CHOICES)
    scholarship_classification = models.CharField(max_length=10, null=True, blank=True, choices=SCHOLARSHIIP_CLASSIFICATION)
    scholarship_classification_other = models.CharField(max_length=100, null=True, blank=True)
    had_rural_training = models.CharField(max_length=3, null=True, blank=True, choices=YES_NO_CHOICES)
    rural_traning_type = models.CharField(max_length=15, choices=RURAL_TRAINING_TYPE_CHOICES, null=True, blank=True)
    rural_experience_description = models.TextField(max_length=200, null=True, blank=True)
    have_role_model_mentor = models.CharField(max_length=3, null=True, blank=True, choices=YES_NO_CHOICES)
    role_model_mentor_position = models.CharField(max_length=100, null=True, blank=True)
    role_model_mentor_location = models.CharField(max_length=200, null=True, blank=True)
    internship_type = models.CharField(max_length=10, null=True, blank=True, choices=INTERNSHIP_TYPES)
    internship_department = models.CharField(max_length=100, null=True, blank=True)
    internship_type_other = models.CharField(max_length=100, null=True, blank=True)
    had_training_outside_PH = models.BooleanField(default=False)
    training_outside_PH_location = models.CharField(max_length=200, null=True, blank=True)
    did_rural_living_interest_change = models.BooleanField(default=False)
    rural_living_interest_change_reason = models.CharField(max_length=100, null=True, blank=True)
    rural_living_interest_change_when = models.CharField(max_length=15, choices=INTEREST_CHANGE_WHEN_CHOICES, null=True, blank=True)
    did_rural_practice_interest_change = models.BooleanField(default=False)
    rural_practice_interest_change_reason = models.CharField(max_length=100, null=True, blank=True)
    rural_practice_interest_change_when = models.CharField(max_length=15, choices=INTEREST_CHANGE_WHEN_CHOICES, null=True, blank=True)
    is_inclined_to_medical_practice = models.BooleanField(default=True)
    is_inclined_to_medical_practice_reason = models.CharField(max_length=100, null=True, blank=True)
    suggestion = models.TextField(max_length=500, null=True, blank=True)
    participant = models.ForeignKey('Participant', null=True, blank=True)

    class Meta:
        verbose_name = u'Survey Data'
        verbose_name_plural = u'Survey Data'


    def __unicode__(self):
        return 'Record %s' % self.id


# Author: Rescian Rey
# Description: Contains information about the school of the children of the participant
#                 It is related to the survey data.
# History:
#     10.30.2014  Rescian Rey  Created.
class Child_School(models.Model):
    SCHOOL_TYPE_CHOICES = (
            (None, ''),
            ('public', 'Public'),
            ('private', 'Private')
        )
    survey_data = models.ForeignKey(Survey_Data, related_name='children')
    child_birth_order = models.IntegerField()
    age = models.IntegerField()
    level = models.CharField(max_length=50)
    school_work_location = models.CharField(max_length=200, null=True, blank=True)
    school_type = models.CharField(max_length=7, choices=SCHOOL_TYPE_CHOICES)

    class Meta:
        verbose_name = u'Child School'

# Author: Rescian Rey
# Description: Guardian/Parent object. Parents/Guardians of the participant
# History:
#     10.30.2014  Rescian Rey    Created
class Guardian(models.Model):
    RELATIONSHIP_CHOICES = (
            (None, ''), 
            ('mother', 'Mother'),
            ('father', 'Father'),
            ('guardian', 'Guardian'),
        )
    survey_data = models.ForeignKey(Survey_Data, related_name='guardians')
    relationship = models.CharField(max_length=50, choices=RELATIONSHIP_CHOICES)
    occupation = models.CharField(max_length=50)
    work_location = models.CharField(max_length=200)
    residence_address = models.CharField(max_length=200)


# Author: Rescian Rey
# Description: Sibling Object. Siblings of the participant
# History:
#     10.30.2014    Rescian Rey  Created
class Sibling(models.Model):
    SCHOOL_TYPE_CHOICES = (
            ('public', 'Public'),
            ('private', 'Private')
        )
    survey_data = models.ForeignKey(Survey_Data, related_name='siblings')
    birth_order = models.IntegerField()
    name = models.CharField(max_length=100, null=True, blank=True)
    age = models.IntegerField()
    school_or_occupation = models.CharField(max_length=50)
    school_or_occupation_location = models.CharField(max_length=200)
    school_type = models.CharField(max_length=8, choices=SCHOOL_TYPE_CHOICES)


# Author: Rescian Rey
# Description: Training object. Data about the trainings taken by participant.
# History:
#     10.30.2014 Rescian Rey  Created.
class Training(models.Model):
    TRAINING_TYPE_CHOICES = (
            ('residency', 'Residency'),
            ('fellowship', 'Fellowship'),
        )
    survey_data = models.ForeignKey(Survey_Data, related_name='trainings')
    location = models.CharField(max_length=200)
    inclusive_years_from = models.CharField(max_length=4, validators=[
        RegexValidator(
            regex='^\d{4}$',
            message='Year must be numeric.',
            code='invalid_year'
        )])
    inclusive_years_to = models.CharField(max_length=4, validators=[
        RegexValidator(
            regex='^\d{4}$',
            message='Year must be numeric.',
            code='invalid_year'
        )])
    training_type = models.CharField(max_length=50, choices=TRAINING_TYPE_CHOICES)

# Author: Rescian Rey
# Description: Occupation data
# History:
# 10.30.2014 Rescian Rey Created.
class Occupation(models.Model):
    survey_data = models.ForeignKey(Survey_Data, related_name='occupations')
    description = models.CharField(max_length=100)
    inclusive_years_from = models.CharField(max_length=4, validators=[
        RegexValidator(
            regex='^\d{4}$',
            message='Year must be numeric.',
            code='invalid_year'
        )])
    inclusive_years_to = models.CharField(max_length=4, validators=[
        RegexValidator(
            regex='^\d{4}$',
            message='Year must be numeric.',
            code='invalid_year'
        )])
    location = models.CharField(max_length=200)


# Author: Rescian Rey
# Description: Education data
# History:
# 10.31.2014  Rescian Rey    Created.
class Education(models.Model):
    SCHOOL_TYPE_CHOICES = (
            ('public', 'Public'),
            ('private', 'Private')
        )
    SCHOOL_LEVEL = (
            ('Elementary', 'Elementary'),
            ('High School', 'High School'),
            ('College', 'College'),
            ('Post Graduate', 'Post Graduate')
        )
    SCHOLARCHIP_TYPE = (
            ('Academic-based', 'Academic-based'),
            ('Non-academic (athletic)', 'Non-academic (athletic)'),
            ('Non-academic (cultural)', 'Non-academic (cultural)'),
            ('Non-academic (other)', 'Non-academic (other)')
        )
    survey_data = models.ForeignKey(Survey_Data, related_name='education')
    school_name = models.CharField(max_length=50)
    level = models.CharField(max_length=200, choices=SCHOOL_LEVEL)
    location = models.CharField(max_length=200)
    school_type = models.CharField(max_length=7, choices=SCHOOL_TYPE_CHOICES)
    scholarhip_type = models.CharField(max_length=50, choices=SCHOLARCHIP_TYPE, null=True, blank=True)

    class Meta:
        verbose_name = u'Education'
        verbose_name_plural = u'Education'


# Author: Rescian Rey
# Description: Data of relatives with business
# History:
# 10.31.2014  Rescian Rey  Created.
class Relative_With_Business(models.Model):
    survey_data = models.ForeignKey(Survey_Data, related_name='relatives_with_business')
    relationship = models.CharField(max_length=50)
    nature_of_business = models.CharField(max_length=100)
    business_size = models.CharField(max_length=50)
    location = models.CharField(max_length=200)

    class Meta:
        verbose_name = u'Relative with Business'
        verbose_name_plural = u'Relatives with Business'


# Author: Rescian Rey
# Description: Data of relatives in Health Professions
# History:
# 10.31.2014    Rescian Rey    Created.
class Relative_In_Health_Profession(models.Model):
    survey_data = models.ForeignKey(Survey_Data, related_name='relatives_in_health_profession')
    relationship = models.CharField(max_length=50)
    occupation = models.CharField(max_length=50)
    location = models.CharField(max_length=200)

    class Meta:
        verbose_name = u'Relative in Health Profession'
        verbose_name_plural = u'Relatives in Health Profession'


# Author: Rescian Rey
# Description: Data of relatives in Politics
# History:
# 10.31.2014  Rescian Rey Created.
class Relative_In_Politics(models.Model):
    survey_data = models.ForeignKey(Survey_Data, related_name='relatives_in_politics')
    relationship = models.CharField(max_length=50)
    position_held = models.CharField(max_length=50)
    location = models.CharField(max_length=200)

    class Meta:
        verbose_name = u'Relative in Politics'
        verbose_name_plural = u'Relatives in Politics'


# Author: Rescian Rey
# Description: Data of relatives with stocks
# History:
# 10.31.2014 Rescian Rey Created.
class Relative_With_Stocks(models.Model):
    survey_data = models.ForeignKey(Survey_Data, related_name='relatives_with_stocks')
    relationship = models.CharField(max_length=50)
    hospital_location = models.CharField(max_length=200)
    is_outside_PH = models.BooleanField(default=False)

    class Meta:
        verbose_name = u'Relative with Stocks'
        verbose_name_plural = u'Relatives with Stocks'

# Author: Rescian Rey
# Description: Data of relatives outside PH
# History:
# 10.31.2014 Rescian Rey Created.
class Relative_Outside_PH(models.Model):
    survey_data = models.ForeignKey(Survey_Data, related_name='relatives_outside_ph')
    relationship = models.CharField(max_length=50)
    location = models.CharField(max_length=200)

    class Meta:
        verbose_name = u'Relative Outside PH'
        verbose_name_plural = u'Relatives Outside PH'


# Author: Rescian Rey
# Description: Participant object
# History:
# 11.1.2014 Rescian Rey     Created.
class Participant(models.Model):
    user = models.OneToOneField(User, null=True, blank=True)
    token = models.CharField(max_length=10)
    active = models.BooleanField(default=False)
    email = models.EmailField()
    mobile = models.CharField(max_length=13,validators=[
        RegexValidator(
            regex='^\+\d{12}$',
            message='Mobile number must be numeric. No spaces.',
            code='invalid_mobile'
        )])
    has_submitted = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s' % self.email

# Author: Rescian Rey
# Description: Specialties 
# History:
# 02.13.2015 Rescian Rey Created
class Specialty(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Specialties'

    def __unicode__(self):
        return '%s' % self.name


# Author: Rescian Rey
# Description: Leisure Activities 
# History:
# 02.13.2015 Rescian Rey Created
class Leisure(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'Leisure'
        verbose_name_plural = 'Leisure Activities'

    def __unicode__(self):
        return '%s' % self.name