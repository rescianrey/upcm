# Author: Rescian Rey
# Description: Utility class for views

import string
import random

from django.forms.models import modelformset_factory
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.template import Context
from django.template.loader import get_template

from app.models import Participant, Survey_Data

from survey.settings import (PARTICIPANT_TOKEN_LENGTH, SEND_SMS, APP_CONFIG, HOSTNAME)

# Author: Rescian Rey
# Description: Random String generator
def generate_passcode():
    duplicate = True
    pc = ''
    while duplicate:
        pc = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(PARTICIPANT_TOKEN_LENGTH))
        duplicate_passcode = Participant.objects.filter(token=pc)
        duplicate = duplicate_passcode.exists()
    return pc

# Author: Rescian Rey
# Description: Creates a formset given a form, and help text
def create_formset(form, help_text=''):
    formset = modelformset_factory(form.Meta.model, form=form, can_delete=True)
    formset.help_text = help_text
    return formset


# Author: Rescian Rey
# Description: Custom login
def login(request, user):
    user.backend='django.contrib.auth.backends.ModelBackend'
    u = auth_login(request, user)
    return u


# Author: Rescian Rey
# Description: Sends SMS
def send_sms(message, number):
    if SEND_SMS:
        from nexmo import send_message
        try:
            status = send_message(to=number, message=message)
            print status
        except Exception, e:
            print e
    print "MESSAGE: %s \nNUMBER: %s" % (message, number)


def send_credentials(participant, first_time=True):
        EMAIL_NOTIF_CONF = {
            'subject': 'Passcode',
            'from_email': 'UPCM@%s' % HOSTNAME,
            'template': 'app/email/passcode_notification.txt'
        }

        SMS_TEMPLATE = 'app/sms/passcode_notification.txt'
        TOKEN_SPLIT = (PARTICIPANT_TOKEN_LENGTH - 2, 2);

        # Email passcode
        email_template = get_template(EMAIL_NOTIF_CONF['template'])
        email_context = Context({
                'site_name': APP_CONFIG['SITE_NAME'],
                'passcode': participant.token[:TOKEN_SPLIT[0]],
                'split': TOKEN_SPLIT[0],
                'login_url': '%s%s ' % (HOSTNAME, reverse('login')),
                'from_name': '%s Team' % APP_CONFIG['SITE_NAME'],
                'first_time': first_time,
            })

        email_text = email_template.render(email_context)
        send_mail(EMAIL_NOTIF_CONF['subject'], email_text, EMAIL_NOTIF_CONF['from_email'],
            [participant.email], fail_silently=False)

        # SMS the last two characters of the passcode
        sms_template = get_template(SMS_TEMPLATE)
        sms_context = Context({
                'from_name': '%s Team' % APP_CONFIG['SITE_NAME'],
                'passcode': participant.token[-TOKEN_SPLIT[1]:],
                'login_url': '%s%s ' % (HOSTNAME, reverse('login')),
                'split': TOKEN_SPLIT[1],
            })
        sms_content = sms_template.render(sms_context)
        send_sms(sms_content, participant.mobile)

# Author: Rescian Rey
# Description: Activates the participant by creating a corresponding Django user and setting the field 'active' to True
def activate_participant(participant):
    # Create a user for the participant
    user, created = User.objects.get_or_create(email=participant.email, username=participant.email)
    user.save()

    participant.active = True
    participant.user = user
    participant.save()


# Author: Rescian Rey
# Description: Gets the survey data of the currently logged in user. If there's no survey data record yet, it will create one
def get_survey(request):
    participant = request.user.participant
    survey, created = Survey_Data.objects.get_or_create(participant=participant)

    return survey