from django.core.management.base import BaseCommand
from django.core.mail import send_mail, send_mass_mail
from django.core.urlresolvers import reverse
from django.template import Context
from django.template.loader import get_template

from app.models import Participant
from app.views_utils import send_sms

from survey import settings

# Author: Rescian Rey
# Description: Sends out notifications for participants that have not submitted their survey forms.
class Command(BaseCommand):
	SMS_TEMPLATE = 'app/sms/submission_notification.txt'
	EMAIL_TEMPLATE = 'app/email/submission_notification.txt'
	EMAIL_SUBJECT = 'Pending Survey Submission'
	EMAIL_FROM = 'UPCM@%s' % settings.HOSTNAME

	CONTEXT = {
		'site_link': '%s%s' % (settings.HOSTNAME, reverse('home')),
		'from_name': 'UPCM Survey Team'
	}

	def handle(self, *args, **options):
		pending_participants = Participant.objects.filter(active=True, has_submitted=False)

		# Construct sms content
		sms_template = get_template(self.SMS_TEMPLATE)
		sms_context = Context(self.CONTEXT)
		sms_content = sms_template.render(sms_context)

		# Construct email content
		email_template = get_template(self.EMAIL_TEMPLATE)
		email_context = Context(self.CONTEXT)
		email_content = email_template.render(email_context)

		mails = []
		for participant in pending_participants:
			send_sms(sms_content, participant.mobile)
			mail_meta = (self.EMAIL_SUBJECT, email_content, self.EMAIL_FROM, [participant.email])
			mails.append(mail_meta)

		send_mass_mail(mails)
		print "Operation successful!"